//
//  DZMReadController.swift
//  DZMeBookRead
//
//  Created by 邓泽淼 on 2017/5/11.
//  Copyright © 2017年 DZM. All rights reserved.
//

import UIKit

@objcMembers
class DZMReadController: DZMViewController,DZMReadMenuDelegate,DZMCoverControllerDelegate,UIPageViewControllerDelegate,UIPageViewControllerDataSource, UIAlertViewDelegate,DZMAppDelegate {
    
    /// 阅读模型(必传)
    var readModel:DZMReadModel!
    
    /// 开启长按菜单
    var openLongMenu:Bool = true
    
    /// 阅读菜单UI
    private(set) var readMenu:DZMReadMenu!
    
    /// 阅读操作对象
    private(set) var readOperation:DZMReadOperation!
    
    /// 翻页控制器 (仿真)
    private(set) var pageViewController:UIPageViewController?
    
    /// 翻页控制器 (无效果,覆盖,上下)
    private(set) var coverController:DZMCoverController?
    
    /// 当前显示的阅读控制器
    private(set) var currentReadViewController:DZMReadViewController?
    
    var listenController:XSListenController?
    
    var currentBookCoverImageUrl:String? = nil
    
    var currentBookAuthor:String? = nil
    
    var currentBookName:String? = nil
    
    ///是否正在下载
    var isDownloadNovel:Bool = false
    var isAlwayBunderAd: Bool = false
    var videoAdIsFinish:Bool = false
    
    //是否已经付费
    var isRemoveADInvold = UserDefaults.standard.bool(forKey: "RADINVILD") as Bool
    
    override var prefersStatusBarHidden: Bool {
        
        return true
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // 设置白色状态栏
        isStatusBarLightContent = true
        
        // 初始化控制器操作对象
        readOperation = DZMReadOperation(vc: self)
        
        // 初始化阅读UI控制对象
        readMenu = DZMReadMenu.readMenu(vc: self, delegate: self)
        
        // 初始化控制器
        creatPageController(readOperation.GetCurrentReadViewController(isUpdateFont: true, isSave: true))
        
        // 注册 DZMReadView 手势通知
        DZMReadView.RegisterNotification(observer: self, selector: #selector(readViewNotification(notification:)))
        
        //默认加载第一个章节内容
        
        MBProgressHUD.showMessage("正在加载第一章节...", to: self.view)
        
        readOperation.loadChapterModelContent(chapterListID: "1") { (success) in
            
            if (success == true) {
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
            }else {
                
                MBProgressHUD.hide(for: self.view, animated: true)
                MBProgressHUD.showHitMessage("章节加载失败,您可以尝试点击右上角切换数据源", to: self.view)
                
            }
        }
        
        if !UserDefaults.standard.bool(forKey: "RADINVILD") {
            addAdView()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeAdFromView(_:)), name: NSNotification.Name(rawValue: "notif_remove_ad"), object: nil)
        
        forceUpdateNovelRead();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: navigation_theme_color)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // 设置回调代理
        (UIApplication.shared.delegate as! YDAppDelegate).delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        readModel.save()
        // 设置回调代理
        (UIApplication.shared.delegate as! YDAppDelegate).delegate = nil
    }
    
    func removeAdFromView(_ noti : Notification) -> Void {
        if noti.object as! Bool {
            //移除广告
            removeAdViewFromSelfView();
        }else{
            //添加广告
            addAdView()
        }
        forceUpdateNovelRead();
    }
    
    func removeAdViewFromSelfView() -> Void {
        var adView = self.view.viewWithTag(10010)
        if adView != nil {
            adView?.removeFromSuperview()
            adView = nil
            removeAdViewFromSelfView();
        }
    }
    
    func forceUpdateNovelRead() -> Void {
        if (currentReadViewController?.readRecordModel != nil) {
            currentReadViewController?.readRecordModel.fouceUpdateFont(isSave: true)
            currentReadViewController?.setBottomStatusViewFrame()
            if (currentReadViewController?.readRecordModel.readChapterModel != nil){
                _ = readOperation.GoToChapter(chapterID: (currentReadViewController?.readRecordModel.readChapterModel?.id)!, toPage: currentReadViewController?.readRecordModel.page as! NSInteger)
            }
        }
    }
    
    func addAdView() -> Void {
        
        if self.view.viewWithTag(10010) == nil {
            let advertisementAdView:XSAdView = XSAdView()
            advertisementAdView.tag = 10010
            advertisementAdView.isAwalyAd = true
            advertisementAdView.backgroundColor = UIColor.clear
            advertisementAdView.frame = CGRect.init(x: 0, y: ScreenHeight-GetCurrentAdViewHeight(), width: ScreenWidth, height: GetCurrentAdViewHeight())
            self.view.insertSubview(advertisementAdView, belowSubview: readMenu.lightButton)
            advertisementAdView.adType = "banner"
            advertisementAdView.adUnitID = "ca-app-pub-7418367853869463/8729919103"
            advertisementAdView.showBunnerView()
        }
    }
    
    // MARK: -- HJAppDelegate 保存阅读记录
    
    /// app 即将退出
    func applicationWillTerminate(_ application: UIApplication) {
        
        readModel.save()
        ReadTmpKeyedRemoveArchiver()
        
    }
    
    /// app 内存警告可能要终止程序
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        
        readModel.save()
        
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication!) {
        ReadTmpKeyedRemoveArchiver()
    }
    
    // MARK: DZMReadView 手势通知
    
    /// 收到通知
    @objc func readViewNotification(notification:Notification) {
        
        // 获得状态
        let info = notification.userInfo
        
        // 隐藏菜单
        readMenu.menuSH(isShow: false)
        
        // 解析状态
        if info != nil && info!.keys.contains(DZMKey_ReadView_Ges_isOpen) {
            
            let isOpen = info![DZMKey_ReadView_Ges_isOpen] as! NSNumber
            
            coverController?.gestureRecognizerEnabled = isOpen.boolValue
            
            pageViewController?.gestureRecognizerEnabled = isOpen.boolValue
            
            readMenu.singleTap.isEnabled = isOpen.boolValue
        }
    }
    
    //设置是否临时缓存
    
    func setReadModelIsTempCache(_ isTemp: Bool) -> Void {
        
        SetReadIsTempCache(isTemp)
        
    }
    
    //下载
    
    func startDownloadNovels() -> Void {
        //MBProgressHUD.showMessage("下载存成章节文件,进入沙河看下文件格式")
        addNovelToBookrack()
        DispatchQueue.global().async(execute: {
            let waveProgress:XLWaveProgress? = XLWaveProgress.init(frame: CGRect(x:0,y:0,width:200,height:200))
            waveProgress?.center = self.view.center
            waveProgress?.progress = 0.0
            waveProgress?.alpha = 0
            waveProgress?.tag = 20170612
            DispatchQueue.main.async(execute: {
                self.view.addSubview(waveProgress!)
                let closeProgressBtn:UIButton = UIButton.init(type: .custom)
                closeProgressBtn.frame = CGRect.init(x: 0, y: 0, width: 200, height: 200)
                closeProgressBtn.backgroundColor = UIColor.clear
                closeProgressBtn.addTarget(self, action: #selector(self.hideProgressView), for: .touchUpInside)
                waveProgress?.addSubview(closeProgressBtn)
                
                UIView.animate(withDuration: 1.0, animations: {
                    waveProgress?.alpha = 1
                })
            })
            self.isDownloadNovel = true
            weak var tpself = self
            self.readModel.downloadAllChapter(progressCall: { (progress) in
                let strongSelf = tpself
                if (strongSelf == nil) {return ;}
                DispatchQueue.main.async(execute: {
                    waveProgress?.progress = CGFloat(progress)
                    strongSelf?.readMenu?.progressLine?.frame = CGRect.init(x: 0, y: (strongSelf?.view.bounds.size.height)!-3, width: (strongSelf?.view.bounds.size.width)!*CGFloat(progress), height: 3)
                    if(Int(progress) == 1){
                        if((strongSelf) != nil) {strongSelf?.isDownloadNovel = false};
                        UIView.animate(withDuration: 1.5, animations: {
                            waveProgress?.alpha = 0
                            strongSelf?.readMenu?.progressLine?.alpha = 0
                        }, completion: { (finish) in
                            waveProgress?.removeFromSuperview()
                            strongSelf?.readMenu?.progressLine?.removeFromSuperview()
                            strongSelf?.readMenu?.progressLine = nil
                            MBProgressHUD.showHitMessage("所有章节已全部缓存完成")
                        })
                    }
                })
            })
        })
    }
    
    @objc func hideProgressView() -> Void {
        let waveProgress:XLWaveProgress? = self.view.viewWithTag(20170612) as? XLWaveProgress;
        if waveProgress != nil {
            UIView.animate(withDuration: 0.56, animations: {
                waveProgress?.frame = CGRect.init(x: 0, y: self.view.bounds.size.height-3, width: self.view.bounds.size.width*(waveProgress?.progress)!, height: 3)
            }, completion: { (finish) in
                if(self.readMenu.progressLine == nil){
                    self.readMenu.progressLine = UIView()
                    self.view.addSubview(self.readMenu.progressLine!)
                }
                self.readMenu.progressLine?.frame = CGRect.init(x: 0, y: self.view.bounds.size.height-3, width: self.view.bounds.size.width*(waveProgress?.progress)!, height: 3)
                self.readMenu.progressLine?.backgroundColor = UIColor.init(hex: "#2160ee")
                waveProgress?.removeFromSuperview()
            })
        }
    }
    
    // MARK: -- DZMReadMenuDelegate
    
    /// 背景颜色
    func readMenuClickSetuptColor(readMenu: DZMReadMenu, index: NSInteger, color: UIColor) {
        
        DZMReadConfigure.shared().colorIndex = index
        
        currentReadViewController?.configureBGColor()
    }
    
    /// 翻书动画
    func readMenuClickSetuptEffect(readMenu: DZMReadMenu, index: NSInteger) {
        
        DZMReadConfigure.shared().effectType = index
        
        creatPageController(readOperation.GetCurrentReadViewController())
    }
    
    /// 字体
    func readMenuClickSetuptFont(readMenu: DZMReadMenu, index: NSInteger) {
        
        DZMReadConfigure.shared().fontType = index
        
        creatPageController(readOperation.GetCurrentReadViewController(isUpdateFont: true, isSave: true))
    }
    
    /// 字体大小
    func readMenuClickSetuptFontSize(readMenu: DZMReadMenu, fontSize: CGFloat) {
        
        // DZMReadConfigure.shared().fontSize = fontSize 内部已赋值
        
        creatPageController(readOperation.GetCurrentReadViewController(isUpdateFont: true, isSave: true))
    }
    
    /// 点击书签列表
    func readMenuClickMarkList(readMenu: DZMReadMenu, readMarkModel: DZMReadMarkModel) {
        
        /*
         网络小说操作提示:
         
         1. 判断书签指定章节 是否存在本地缓存文件
         
         2. 存在则继续展示 不存在则请求回来再展示
         */
        
        readModel.modifyReadRecordModel(readMarkModel: readMarkModel, isUpdateFont: true, isSave: false)
        
        creatPageController(readOperation.GetCurrentReadViewController(isUpdateFont: false, isSave: true))
    }
    
    /// 下载
    func readMenuClickDownload(readMenu: DZMReadMenu) {
        if ((UserDefaults.standard.bool(forKey: "loadDownloadNovelAd") == true) && videoAdIsFinish == false) {
            let adView:XSAdView? = self.view.viewWithTag(10010) as? XSAdView
            if adView != nil {
                weak var tpself = self
                adView?.showVideoView({ (finish) in
                    tpself?.videoAdIsFinish = finish
                    if(finish == true){
                        tpself?.startDownloadNovels()
                        tpself?.videoAdIsFinish = false
                    }
                })
                return;
            }
        }
        self.startDownloadNovels()
    }
    
    /// 拖拽进度条
    func readMenuSliderEndScroll(readMenu: DZMReadMenu, slider: ASValueTrackingSlider) {
        
        if readModel != nil && readModel.readRecordModel.isRecord { // 有阅读记录
   
            let toPage = NSInteger(slider.value)
     
            if (readModel.readRecordModel.page.intValue + 1) != toPage { // 不是同一页
                
                let _ = readOperation.GoToChapter(chapterID: readModel.readRecordModel.readChapterModel!.id, toPage: toPage - 1)
            }
        }
    }
    
    /// 上一章
    func readMenuClickPreviousChapter(readMenu: DZMReadMenu) {
        
        if readModel != nil && readModel.readRecordModel.isRecord { // 有阅读记录
        
            let _ = readOperation.GoToChapter(chapterID: "\(readModel.readRecordModel.readChapterModel!.id.integerValue() - 1)")
        }
    }
    
    /// 下一章
    func readMenuClickNextChapter(readMenu: DZMReadMenu) {
        
        if readModel != nil && readModel.readRecordModel.isRecord { // 有阅读记录
            
            let _ = readOperation.GoToChapter(chapterID: "\(readModel.readRecordModel.readChapterModel!.id.integerValue() + 1)")
        }
    }
    
    /// 点击章节列表
    func readMenuClickChapterList(readMenu: DZMReadMenu, readChapterListModel: DZMReadChapterListModel) {
        
        let _ = readOperation.GoToChapter(chapterID: readChapterListModel.id)
    }
    
    /// 切换日夜间模式
    func readMenuClickLightButton(readMenu: DZMReadMenu, isDay: Bool) {
        
        // 日夜间需要切换做调整可以打开重置使用
        // creatPageController(readOperation.GetCurrentReadViewController())
    }
    
    /// 状态栏 将要 - 隐藏以及显示状态改变
    func readMenuWillShowOrHidden(readMenu: DZMReadMenu, isShow: Bool) {
        
        pageViewController?.tapGestureRecognizerEnabled = !isShow
        
        coverController?.tapGestureRecognizerEnabled = !isShow
        
        if isShow {
            
            // 选中章节列表
            readMenu.leftView.topView.selectIndex = 0
            
            // 检查当前是否存在书签
            readMenu.topView.mark.isSelected = readModel.checkMark()
        }
    }
    
    /// 点击书签按钮
    func readMenuClickMarkButton(readMenu: DZMReadMenu, button: UIButton) {
        
        if button.isSelected {
            
            let _ = readModel.removeMark()
            
            button.isSelected = readModel.checkMark()
            
        }else{
            
            readModel.addMark()
            
            button.isSelected = true
        }
    }
    
    // MARK: -- 创建 PageController
    
    /// 创建效果控制器 传入初始化显示控制器
    func creatPageController(_ displayController:UIViewController?) {
        
        // 清理
        if pageViewController != nil {
            
            pageViewController?.view.removeFromSuperview()
            
            pageViewController?.removeFromParentViewController()
            
            pageViewController = nil
        }
        
        if coverController != nil {
            
            coverController?.view.removeFromSuperview()
            
            coverController?.removeFromParentViewController()
            
            coverController = nil
        }
        
        // 创建
        if DZMReadConfigure.shared().effectType == DZMRMEffectType.simulation.rawValue { // 仿真
            
            let options = [UIPageViewControllerOptionSpineLocationKey:NSNumber(value: UIPageViewControllerSpineLocation.min.rawValue as Int)]
            
            pageViewController = UIPageViewController(transitionStyle:UIPageViewControllerTransitionStyle.pageCurl,navigationOrientation:UIPageViewControllerNavigationOrientation.horizontal,options: options)
            
            pageViewController!.delegate = self
            
            pageViewController!.dataSource = self
            
            // 为了翻页背面的颜色使用
            pageViewController!.isDoubleSided = true
            
            view.insertSubview(pageViewController!.view, at: 0)
            
            addChildViewController(pageViewController!)
            
            pageViewController!.setViewControllers((displayController != nil ? [displayController!] : nil), direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            
        }else{ // 无效果 覆盖 上下
            
            coverController = DZMCoverController()
            
            coverController!.delegate = self
            
            view.insertSubview(coverController!.view, at: 0)
            
            addChildViewController(coverController!)
            
            coverController!.setController(displayController)
            
            if DZMReadConfigure.shared().effectType == DZMRMEffectType.none.rawValue {
                
                coverController!.openAnimate = false
                
            }else if DZMReadConfigure.shared().effectType == DZMRMEffectType.upAndDown.rawValue {
                
                coverController!.openAnimate = false
                
                coverController!.gestureRecognizerEnabled = false
            }
        }
        
        // 记录
        currentReadViewController = displayController as? DZMReadViewController
    }
    
    /// 翻页操作使用 isAbove: true 上一页; false 下一页;
    func setViewController(displayController:UIViewController?, isAbove: Bool, animated: Bool) {
        
        if displayController != nil {
            
            if pageViewController != nil {
                
                let direction = isAbove ? UIPageViewControllerNavigationDirection.reverse : UIPageViewControllerNavigationDirection.forward
                
                pageViewController?.setViewControllers([displayController!], direction: direction, animated: animated, completion: nil)
                
                return
            }
            
            if coverController != nil {
                
                coverController?.setController(displayController!, animated: animated, isAbove: isAbove)
                
                return
            }
            
            // 都没有则初始化
            creatPageController(displayController!)
        }
    }
    
    func autoChangeNovelsOrigin(chapterListID:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: "切换小说数据源", message: "没有更多章节内容,需要切换来获取更多章节吗?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction.init(title: "关闭", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction.init(title: "去换源", style: .default, handler: { (action) in
                self.toLoadNewBookOrigins(chapterListID)
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func preAutoChangeNovelsOrigin(chapterListID:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: "切换小说数据源", message: "下一章将没有章节内容，需要提前切换数据源来获取更多章节吗?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction.init(title: "不需要", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction.init(title: "去换源", style: .default, handler: { (action) in
                self.toLoadNewBookOrigins(chapterListID)
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: -- UIAlertViewDelegate
    
    func alertView(_ alertView: UIAlertView, willDismissWithButtonIndex buttonIndex: Int) {
        if alertView.buttonTitle(at: buttonIndex) == "去换源" {
           
        }else if alertView.buttonTitle(at: buttonIndex) == "确定" {
            self.addNovelToBookrack()
            let _ = self.navigationController?.popViewController(animated: true)
        }else if alertView.buttonTitle(at: buttonIndex) == "返回" {
            let _ = self.navigationController?.popViewController(animated: true)
        }else if alertView.buttonTitle(at: buttonIndex) == "继续下载" {
            
        }
    }
    
    func addNovelToBookrack() -> Void {
        if GetReadIsTempCache() {
//            CopyFromTmpCacheDir(readModel.bookID)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notif_add_bookrack"), object: nil);
        }
    }
    
    func StartVoice(_ text: String?) -> Void {
        
        //在此添加语音合成
        var currentChapterModel:DZMReadChapterModel? = nil
        if currentReadViewController != nil {
            currentChapterModel = currentReadViewController?.readRecordModel.readChapterModel
        }
        if currentChapterModel != nil {
            let currentPageString = currentChapterModel?.string(page: currentReadViewController?.readRecordModel?.page as! NSInteger)
            if (listenController == nil) {
                listenController = XSListenController.init(nibName: "XSListenController", bundle: nil)
                listenController?.vcController = self;
            }
            listenController?.title = currentChapterModel?.name
            listenController?.currentPageCount = currentChapterModel?.pageCount as! Int
            listenController?.currentChapterIdNum = currentChapterModel?.id.integerValue() ?? 0
            listenController?.currentPageIndex = currentReadViewController?.readRecordModel.page as! Int
            if(listenController?.isListening == false){
                navigationController?.pushViewController(listenController!, animated: true)
            }
            listenController?.play(with: currentPageString);
        }
    }
    
    func toLoadNewBookOrigins(_ needChapterId:String) -> Void {
        MBProgressHUD.showMessage("正在加载数据源...", to: self.view);
        XSNetWorking.loadChapterOrigin(self.readModel.bookID, callback: { (originList) in
            if ((originList as? NSArray) != nil){
                let originController = XSOriginsController.init(origins: originList as! [Any], callback: { (chapterList) in
                    if(GetReadIsTempCache()){
                        ReadTmpKeyedRemoveArchiver(self.readModel.bookID);
                    }else{
                        ReadKeyedRemoveArchiver(self.readModel.bookID);
                    }
                    self.readModel = DZMReadModel.readModelWithNetBook(self.readModel.bookID, GetReadIsTempCache())
                    
                    var realChapterId = needChapterId;
                    
                    if (realChapterId.integerValue() >= (chapterList?.count)!){
                        realChapterId = "\(chapterList?.count ?? 0)"
                    }
                    let _ = self.readOperation.GoToChapter(chapterID: realChapterId, toPage: 0)
                });
                originController?.bookId = self.readModel.bookID;
                self.navigationController?.pushViewController(originController!, animated: true)
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                MBProgressHUD.showError("无法获取数据源,点击右上角切源按钮继续寻找新的资源", to: self.view)
            }
        })
    }
    
    // MARK: -- DZMCoverControllerDelegate
    
    /// 切换结果
    func coverController(_ coverController: DZMCoverController, currentController: UIViewController?, finish isFinish: Bool) {
        
        // 记录
        currentReadViewController = currentController as? DZMReadViewController
        
        // 更新阅读记录
        readOperation.readRecordUpdate(readViewController: currentReadViewController)
        
        // 更新进度条
        readMenu.bottomView.sliderUpdate()
    }
    
    /// 将要显示的控制器
    func coverController(_ coverController: DZMCoverController, willTransitionToPendingController pendingController: UIViewController?) {
        
        readMenu.menuSH(isShow: false)
    }
    
    /// 获取上一个控制器
    func coverController(_ coverController: DZMCoverController, getAboveControllerWithCurrentController currentController: UIViewController?) -> UIViewController? {
        
        return readOperation.GetAboveReadViewController()
    }
    
    /// 获取下一个控制器
    func coverController(_ coverController: DZMCoverController, getBelowControllerWithCurrentController currentController: UIViewController?) -> UIViewController? {
        
        return readOperation.GetBelowReadViewController()
    }
    
    // MARK: -- UIPageViewControllerDelegate
    
    /// 切换结果
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if !completed {
            
            // 记录
            currentReadViewController = previousViewControllers.first as? DZMReadViewController
            
            // 更新阅读记录
            readOperation.readRecordUpdate(readViewController: currentReadViewController)
            
        }else{
            
            // 记录
            currentReadViewController = pageViewController.viewControllers?.first as? DZMReadViewController
            
            // 更新阅读记录
            readOperation.readRecordUpdate(readViewController: currentReadViewController)
            
            // 更新进度条
            readMenu.bottomView.sliderUpdate()
        }
    }
    
    /// 准备切换
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        readMenu.menuSH(isShow: false)
        
        // 更新阅读记录
        readOperation.readRecordUpdate(readViewController: pageViewController.viewControllers?.first as? DZMReadViewController, isSave: false)
    }
    
    
    // MARK: -- UIPageViewControllerDataSource
    
    /// 用于区分正反面的值(固定)
    private var TempNumber:NSInteger = 1
    
    /// 获取上一页
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let readviewController = readOperation.GetAboveReadViewController()
        
        if readviewController == nil {
            
            return nil
            
        }
        
        TempNumber -= 1
        
        if abs(TempNumber) % 2 == 0 { // 背面
            
            let vc = DZMReadBGController()
            
            vc.targetView = readviewController?.view
            
            return vc
            
        }else{ // 内容
            
            return readviewController
            
        }
    }
    
    /// 获取下一页
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let readviewController = readOperation.GetBelowReadViewController()
        
        if readviewController == nil {
            return nil
        }
        
        TempNumber += 1
        
        if abs(TempNumber) % 2 == 0 { // 背面
            
            let vc = DZMReadBGController()
            
            vc.targetView = readOperation.GetCurrentReadViewController()?.view
            
            return vc
            
        }else{ // 内容
            
            return readviewController
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
        // 移除通知
        DZMReadView.RemoveNotification(observer: self)
        NotificationCenter.default.removeObserver(self)
        // 清理
        readModel = nil
        currentReadViewController = nil
    }
}
