//
//  DZMReadOperation.swift
//  DZMeBookRead
//
//  Created by 邓泽淼 on 2017/5/15.
//  Copyright © 2017年 DZM. All rights reserved.
//

import UIKit

@objcMembers

class DZMReadOperation: NSObject {

    /// 阅读控制器
    weak var vc:DZMReadController!
    
    // MARK: -- init
    
    init(vc:DZMReadController) {
        
        super.init()
        
        self.vc = vc
    }
    
    // MARK: -- 获取阅读控制器 DZMReadViewController
    
    /// 获取阅读View控制器
    func GetReadViewController(readRecordModel:DZMReadRecordModel?) ->DZMReadViewController? {
        
        if readRecordModel != nil {
            
            let readViewController = DZMReadViewController()
            
            readViewController.readRecordModel = readRecordModel
            
            readViewController.readController = vc
            
            if((vc.listenController != nil) && vc.listenController?.isListening == true){
                //开始切换听书内容
                let currentPageString = readRecordModel?.readChapterModel?.string(page: readRecordModel?.page as! NSInteger)
                vc.listenController?.title = readRecordModel?.readChapterModel?.name
                vc.listenController?.currentPageCount = readRecordModel?.readChapterModel?.pageCount as! Int
                vc.listenController?.play(with: currentPageString)
            }
            
            return readViewController
        }
        
        return nil
    }
    
    /// 获取当前阅读记录的阅读View控制器
    func GetCurrentReadViewController(isUpdateFont:Bool = false, isSave:Bool = false) ->DZMReadViewController? {
       
        if isUpdateFont {
           
            vc.readModel.readRecordModel.updateFont(isSave: true)
        }
        
        if isSave {
            
            readRecordUpdate(readRecordModel: vc.readModel.readRecordModel)
        }
        
        return GetReadViewController(readRecordModel: vc.readModel.readRecordModel.copySelf())
    }
    
    /// 获取上一页控制器
    func GetAboveReadViewController() ->DZMReadViewController? {
        
        // 没有阅读模型
        if vc.readModel == nil || !vc.readModel.readRecordModel.isRecord {return nil}
        
        // 阅读记录
        var readRecordModel:DZMReadRecordModel?
        
        // 获得阅读记录
        readRecordModel = vc.readModel.readRecordModel.copySelf()
        
        // 小说ID
        let bookID = vc.readModel.readRecordModel.readChapterModel!.bookID
        
        // 章节ID
        let id = vc.readModel.readRecordModel.readChapterModel!.id.integerValue()
        
        // 页码
        let page = vc.readModel.readRecordModel.page.intValue
        
        // 到头了
        if id == 0 && page == 0 {return nil}
        
        // 判断
        
        if page > 0 {
            
            readRecordModel?.page = NSNumber(value: (page - 1))
            
        } else {
            
            let needLoadChapterId = "\(id - 1)"
            
            if vc.readModel.isLocalBook.boolValue { // 本地小说
                
                readRecordModel?.modify(chapterID: needLoadChapterId, toPage: DZMReadLastPageValue, isUpdateFont:true, isSave: false)
                
            }else{ // 网络小说
                
                /*
                 网络小说操作提示:
                 
                 1. 获得阅读记录
                 
                 2. 获得当前章节ID
                 
                 3. 获得当前阅读章节 读到的页码
                 
                 4. 判断是否为这一章最后一页
                 
                 5. 1). 判断不是第一页则 page - 1 继续翻页
                 2). 如果是第一页则判断上一章的章节ID是否有值,没值就是当前没有跟多章节（连载中）或者 全书完, 有值则判断是否存在缓存文件.
                 有缓存文件则拿出使用更新阅读记录, 没值则请求服务器获取，请求回来之后可动画展示出来
                 
                 提示：如果是请求回来之后并更新了阅读记录 可使用 GetCurrentReadViewController() 获得当前阅读记录的控制器 进行展示
                 */
                
                if (DZMReadChapterModel.IsExistReadChapterModel(bookID: bookID!, chapterID: needLoadChapterId)){
                    readRecordModel?.modify(chapterID: needLoadChapterId, toPage: DZMReadLastPageValue, isUpdateFont:true, isSave: false)
                    disposePreChapter(chapterListID: needLoadChapterId)
                }else{
                    weak var tpself = self
                    MBProgressHUD.showHitMessage("正在加载章节内容", to: vc.currentReadViewController?.view)
                    loadChapterModelContent(chapterListID: needLoadChapterId, finishCallback: { (success) in
                        let strongSelf = tpself
                        MBProgressHUD.hideAllHUDs(for: strongSelf?.vc.currentReadViewController?.view, animated: true)
                        if(success == true){
                            strongSelf?.loadPreChapterContent(chapterListID: needLoadChapterId)
                            readRecordModel?.modify(chapterID: needLoadChapterId, toPage: DZMReadLastPageValue, isUpdateFont:true, isSave: true)
                            let _ = strongSelf?.GetCurrentReadViewController(isUpdateFont: true, isSave: true)
                        }else{
                            strongSelf?.autoChangeNovelsOrigin(chapterListID: needLoadChapterId)
                        }
                    })
                    readRecordModel = nil
                }
            }
        }
        
        return GetReadViewController(readRecordModel: readRecordModel)
    }
    
    
    /// 获得下一页控制器
    func GetBelowReadViewController() ->DZMReadViewController? {
        
        // 没有阅读模型
        if vc.readModel == nil || !vc.readModel.readRecordModel.isRecord {return nil}
        
        // 阅读记录
        var readRecordModel:DZMReadRecordModel?
        
        // 获得阅读记录
        readRecordModel = vc.readModel.readRecordModel.copySelf()
        
        // 小说ID
        let bookID = vc.readModel.readRecordModel.readChapterModel!.bookID
        
        // 章节ID
        let id = vc.readModel.readRecordModel.readChapterModel!.id.integerValue()
        
        // 页码
        let page = vc.readModel.readRecordModel.page.intValue
        
        // 最后一页
        let lastPage = vc.readModel.readRecordModel.readChapterModel!.pageCount.intValue - 1
        
        // 到头了
        if id == vc.readModel.readChapterListModels.count - 1 && page == lastPage {return nil}
        
        if page == lastPage {
            
            let needLoadChapterId = "\(id + 1)"
            
            // 判断
            if vc.readModel.isLocalBook.boolValue { // 本地小说
                readRecordModel?.modify(chapterID: needLoadChapterId, isUpdateFont: true)
            }else{
                if (DZMReadChapterModel.IsExistReadChapterModel(bookID: bookID!, chapterID: needLoadChapterId)){
                    readRecordModel?.modify(chapterID: needLoadChapterId, toPage: 0, isUpdateFont:true, isSave: false)
                    readRecordModel?.fouceUpdateFont();
                    disposeNextChapter(chapterListID: needLoadChapterId)
                }else{
                    weak var tpself = self
                    MBProgressHUD.showHitMessage("正在加载章节内容", to: vc.currentReadViewController?.view)
                    self.loadChapterModelContent(chapterListID: needLoadChapterId, finishCallback: { (success) in
                        let strongSelf = tpself
                        MBProgressHUD.hideAllHUDs(for: strongSelf?.vc.currentReadViewController?.view, animated: true)
                        if(success == true){
                            strongSelf?.loadNextChapterContent(chapterListID: needLoadChapterId)
                            readRecordModel?.modify(chapterID: needLoadChapterId, toPage: DZMReadLastPageValue, isUpdateFont:true, isSave: true)
                            let _ = strongSelf?.GetCurrentReadViewController(isUpdateFont: true, isSave: true)
                        }else{
                            strongSelf?.autoChangeNovelsOrigin(chapterListID: needLoadChapterId)
                        }
                    })
                    readRecordModel = nil
                }
            }
        }else{
            readRecordModel?.page = NSNumber(value: (page + 1))
        }
        
        return GetReadViewController(readRecordModel: readRecordModel)
    }
    
    /// 跳转指定章节 指定页码 (toPage: -1 为最后一页 也可以使用 DZMReadLastPageValue)
    func GoToChapter(chapterID:String, toPage:NSInteger = 0) ->Bool {
        
        if chapterID.integerValue() > vc.readModel.readChapterListModels.count - 1 {
            
            MBProgressHUD.showHitMessage("已经是最后一章啦~")
            
            return false;
        
        }
        
        if chapterID.integerValue() < 0 {
            
            MBProgressHUD.showHitMessage("已经是第一章啦~")
            
            return false
        }
        
        if vc.readModel != nil { // 有阅读模型
            
            if DZMReadChapterModel.IsExistReadChapterModel(bookID: vc.readModel.bookID, chapterID: chapterID) { //  存在
                
                disposeNextChapter(chapterListID: chapterID)
                
                disposePreChapter(chapterListID: chapterID)
                
                vc.readModel.modifyReadRecordModel(chapterID: chapterID, page: toPage, isSave: false)
                
                vc.creatPageController(GetCurrentReadViewController(isUpdateFont: true, isSave: true))
                
                return true
                
            }else{ // 不存在

                /*
                 网络小说操作提示:
                 
                 1. 请求章节内容 并缓存
                 
                 2. 修改阅读记录 并展示
                 */
                
                if vc.readModel.isLocalBook.boolValue { // 本地小说
                    
                }else{ // 网络小说
                    weak var tpself = self;
                    MBProgressHUD.showHitMessage("正在加载章节内容", to: vc.currentReadViewController?.view)
                    self.loadChapterModelContent(chapterListID: chapterID, finishCallback: { (success) in
                        let strongSelf = tpself;
                        MBProgressHUD.hideAllHUDs(for: strongSelf?.vc.currentReadViewController?.view, animated: true)
                        if(success){
                            let _  = strongSelf?.GoToChapter(chapterID: chapterID, toPage: 0)
//                            strongSelf?.loadNextChapterContent(chapterListID: chapterID)
//                            strongSelf?.loadPreChapterContent(chapterListID: chapterID)
                        }else{
                            strongSelf?.autoChangeNovelsOrigin(chapterListID: chapterID)
                        }
                    })
                }
                
                return false
            }
        }
        
        return false
    }
    
    func disposeNextChapter(chapterListID:String) -> Void {
        if vc.readModel.isLocalBook.boolValue == false {
            let chapterIDTmp = chapterListID.integerValue();
            if (chapterIDTmp < (vc.readModel.readChapterListModels.count - 1)) {
                
                let readChapterListModel = vc.readModel.GetReadChapterListModel(chapterID: "\(chapterIDTmp+1)")
                if readChapterListModel?.isDownload == 0 {
                    loadNextChapterContent(chapterListID: chapterListID)
                }
            }
        }
    }
    
    func disposePreChapter(chapterListID:String) -> Void {
        if vc.readModel.isLocalBook.boolValue == false {
            let chapterIDTmp = chapterListID.integerValue();
            if chapterIDTmp > 1 {
                let readChapterListModel = vc.readModel.GetReadChapterListModel(chapterID: "\(chapterIDTmp-1)")
                if readChapterListModel?.isDownload == 0 {
                    loadPreChapterContent(chapterListID: chapterListID)
                }
            }
        }
    }
    
    func loadNextChapterContent(chapterListID:String) -> Void {
        if (vc != nil) {
            var currentIndex = chapterListID.integerValue()
            for _ in 1...5 {
                currentIndex += 1;
                if currentIndex >= vc.readModel.readChapterListModels.count {
                    break;
                }
                self.loadChapterModelContent(chapterListID: "\(currentIndex)", finishCallback: { (success) in
                    
                });
            }
        };
    }
    
    func loadPreChapterContent(chapterListID:String) -> Void {
        var currentIndex = chapterListID.integerValue()
        for _ in 1...2 {
            currentIndex -= 1;
            if currentIndex == 0 {
                break;
            }
            self.loadChapterModelContent(chapterListID: "\(currentIndex)", finishCallback: { (success) in
                
            })
        }
    }
    
    func loadChapterModelContent(chapterListID:String,finishCallback:@escaping ((_ success:Bool)->Void)) -> Void {
        let readChapterListModel = vc.readModel.GetReadChapterListModel(chapterID: chapterListID)
        if (readChapterListModel != nil) {
            self.vc.readModel.downloadOneChapterContent(readChapterListModel: readChapterListModel!) { (success) in
                finishCallback(success)
            }
        }else {
            finishCallback(false)
        }
    }
    
    func autoChangeNovelsOrigin(chapterListID:String) -> Void {
        vc.autoChangeNovelsOrigin(chapterListID: chapterListID)
    }
    
    // MARK: -- 同步记录 
    
    /// 更新记录
    func readRecordUpdate(readViewController:DZMReadViewController?, isSave:Bool = true) {
       
        readRecordUpdate(readRecordModel: readViewController?.readRecordModel, isSave: isSave)
    }
    
    /// 更新记录
    func readRecordUpdate(readRecordModel:DZMReadRecordModel?, isSave:Bool = true) {
        
        if readRecordModel != nil {
            
            vc.readModel.readRecordModel = readRecordModel
            
            if isSave {
                
                // 保存
                vc.readModel.readRecordModel.save()
                
                // 更新UI
                DispatchQueue.main.async { [weak self] ()->Void in
                    
                    // 进度条数据初始化
                    self?.vc.readMenu.bottomView.sliderUpdate()
                }
            }
        }
    }
}
