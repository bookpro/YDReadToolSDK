//
//  DZMRMTopView.swift
//  DZMeBookRead
//
//  Created by 邓泽淼 on 2017/5/11.
//  Copyright © 2017年 DZM. All rights reserved.
//

import UIKit

class DZMRMTopView: DZMRMBaseView {

    /// 返回按钮
    private(set) var back:UIButton!
    
    /// 书签
    private(set) var mark:UIButton!
    
    ///换源
    private(set) var originItem:UIButton!
    
    ///听书
    private(set) var listenItem:UIButton!
    
    override func addSubviews() {
        
        super.addSubviews()
        
        // 返回
        back = UIButton(type:.custom)
        back.setImage(UIImage(named:"G_Back_0"), for: .normal)
        addSubview(back)
        
        // 书签
        mark = UIButton(type:.custom)
        mark.contentMode = .center
        mark.setImage(UIImage(named:"RM_17"), for: .normal)
        mark.setImage(UIImage(named:"RM_18"), for: .selected)
        mark.addTarget(self, action: #selector(DZMRMTopView.clickMark(button:)), for: .touchUpInside)
        addSubview(mark)
        
        listenItem = UIButton(type:.custom)
        listenItem.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        listenItem.setImage(UIImage(named:"icon_erji")!.withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        listenItem.setImage(UIImage(named:"icon_erji")!.withRenderingMode(.alwaysTemplate), for: UIControlState.selected)
        listenItem.tintColor = UIColor.white
        listenItem.contentHorizontalAlignment = .right
        listenItem.addTarget(self, action: #selector(DZMRMTopView.clickBookListen), for: .touchUpInside)
        addSubview(listenItem)
        
        originItem = UIButton(type:.custom)
        originItem.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        originItem.setImage(UIImage(named:"icon_origins")!.withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        originItem.setImage(UIImage(named:"icon_origins")!.withRenderingMode(.alwaysTemplate), for: UIControlState.selected)
        originItem.tintColor = UIColor.white
        originItem.contentHorizontalAlignment = .right
        originItem.addTarget(self, action: #selector(DZMRMTopView.clickBookChangeOrigin), for: .touchUpInside)
        addSubview(originItem)
    }
    
    @objc private func clickMark(button:UIButton) {
        
        readMenu.delegate?.readMenuClickMarkButton?(readMenu: readMenu, button: button)
    }
    
    @objc func clickBookChangeOrigin() {
        
        readMenu.vc.toLoadNewBookOrigins((readMenu.vc.currentReadViewController?.readRecordModel.readChapterModel?.id)!)
    }
    
    @objc func clickBookListen() {
        
        readMenu.vc.StartVoice(nil)
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        // 按钮宽
        let buttonW:CGFloat = 50
        
        originItem.frame = CGRect(x: width - buttonW, y: StatusBarHeight+5, width: 30, height: 30)
        originItem.contentEdgeInsets = UIEdgeInsetsMake(1, 0, 1, 0)
        
        listenItem.frame = CGRect(x: width - buttonW*2, y: StatusBarHeight, width: 38, height: 38)
        
        // 返回按钮
        back.frame = CGRect(x: 0, y: StatusBarHeight, width: buttonW, height: height - StatusBarHeight)
        
        // 书签按钮
        mark.frame = CGRect(x: width - buttonW*3, y: StatusBarHeight, width: buttonW, height: height - StatusBarHeight)
    }
}
