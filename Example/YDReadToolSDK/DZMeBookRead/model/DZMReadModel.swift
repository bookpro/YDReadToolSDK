//
//  DZMReadModel.swift
//  DZMeBookRead
//
//  Created by 邓泽淼 on 2017/5/12.
//  Copyright © 2017年 DZM. All rights reserved.
//

import UIKit

@objcMembers
class DZMReadModel: NSObject,NSCoding {
    
    /// 小说ID
    var bookID:String!
    
    /// 是否为本地小说
    var isLocalBook:NSNumber = NSNumber(value: 0)

    /// 章节列表数组（章节列表不包含章节内容, 它唯一的用处就是在阅读页面给用户查看章节列表）
    var readChapterListModels:[DZMReadChapterListModel] = [DZMReadChapterListModel]()
    
    /// 阅读记录
    var readRecordModel:DZMReadRecordModel!
    
    /// 书签列表
    private(set) var readMarkModels:[DZMReadMarkModel] = [DZMReadMarkModel]()
    
    /// 当前书签(用于记录使用)
    private(set) var readMarkModel:DZMReadMarkModel?
    
    // MARK: -- init
    
    private override init() {
        
        super.init()
    }
    
    convenience init(bookID:String,jsonData:NSArray) {
        self.init()
        
        //        self.content = content
        
        self.bookID = bookID
        
        readChapterListModels = DZMReadParser.getChapterListModels(bookID,GetReadIsTempCache(),jsonData)
        
        XSNetWorking.shareManager().chapterList = nil;
        
        if !readChapterListModels.isEmpty {
            
            if (readRecordModel == nil) {
                
                readRecordModel = DZMReadRecordModel()
                
            }
            
            readRecordModel.readChapterModel = DZMReadParser.getWelcomeChapterModel(bookID);
            
            readRecordModel.bookID = bookID;
            
            readRecordModel.page = 0
            
            // 设置阅读记录 第一个章节 为 首个章节ID
            modifyReadRecordModel(chapterID: (readRecordModel.readChapterModel?.id)!, page: 0, isUpdateFont: true, isSave: true)
            
            save();
        }

    }
    
    /// 获得阅读模型
    class func readModel(bookID:String) ->DZMReadModel {
        
        var readModel:DZMReadModel!
        
        if DZMReadModel.IsExistReadModel(bookID: bookID) { // 存在
            
            readModel = ReadKeyedUnarchiver(bookID, fileName: bookID) as! DZMReadModel
            
        }else{ // 不存在
            
            readModel = DZMReadModel()
            
            readModel.bookID = bookID
            
            readModel.isLocalBook = 1;
        }
        
        // 阅读记录 刷新字体是以防在别的小说修改了字体
        readModel!.readRecordModel = DZMReadRecordModel.readRecordModel(bookID: bookID, isUpdateFont: true, isSave: true)
        
        // 返回
        return readModel!
    }
    
    //网络小说
    class func readModelWithNetBook(_ bookId:String,_ cacheToTmp:Bool) ->DZMReadModel?{
        var model:DZMReadModel? = nil
        
        SetReadIsTempCache(cacheToTmp)
        
        if cacheToTmp {
            model = ReadTmpKeyedUnarchiver(bookId, fileName: bookId) as? DZMReadModel
        }else{
            model = ReadKeyedUnarchiver(bookId, fileName: bookId) as? DZMReadModel
        }
        // 没有缓存
        if model == nil {
            if XSNetWorking.shareManager().chapterList != nil {
                model = DZMReadModel(bookID: bookId, jsonData: XSNetWorking.shareManager().chapterList! as NSArray)
                DZMReadModel.updateReadModel(model!)
            }else{
                return nil;
            }
        }
        model!.readRecordModel = DZMReadRecordModel.readRecordModel(bookID: bookId, isUpdateFont: true, isSave: true)
        
        return model!;
    }
    
    // MARK: -- 操作
    
    // MARK: -- 刷新缓存数据
    
    class func updateReadModel(_ readModel:DZMReadModel) {
        if GetReadIsTempCache() {
            ReadTmpKeyedArchiver(readModel.bookID, fileName: readModel.bookID, object: readModel)
        }else{
            ReadKeyedArchiver(readModel.bookID, fileName: readModel.bookID, object: readModel)
        }
    }
    
    class func updateTmpReadModel(_ readModel:DZMReadModel) {
        
        ReadTmpKeyedArchiver(readModel.bookID, fileName: readModel.bookID, object: readModel)
    }
    
    /// 传入Key 获取对应阅读模型
    class func readModelWithFileName(_ booID:String) ->DZMReadModel? {
        
        return ReadKeyedUnarchiver(booID, fileName: booID) as? DZMReadModel
        
    }
    
    /// MARK: -- 删除书架 - 从本地删除书籍归档
    
    class func MoveNovelFromTempCacheDir(bookId: String) -> Void {
        CopyFromTmpCacheDir(bookId)
    }
    
    class func MoveNovelFromCacheDir(bookId: String) -> Void {
        CopyFromCacheDir(bookId)
    }
    
    class func RemoveBookFromLocal(bookId:String) -> Void {
        var realBookId = bookId;
        if bookId.contains("/") {
            realBookId = DZMReadParser.GetBookName(NSURL.fileURL(withPath: bookId))
        }
        //ReadKeyedRemoveArchiver(realBookId)
        CopyFromCacheDir(realBookId)
    }
    
    //下载网络小说
    
    func downloadAllChapter(progressCall:@escaping ((_ progress:Float) -> Void)) -> Void {
        downloadChapterContent(1, progressCall: progressCall)
    }
    
    func downloadChapterContent(_ index:Int,progressCall:@escaping ((_ progress:Float) -> Void)) -> Void {
        let item = readChapterListModels[index]
        self.downloadOneChapterContent(readChapterListModel: item, finishCallback: { (success) in
            progressCall((Float(index+1)/Float(self.readChapterListModels.count)))
            if(index+1 < self.readChapterListModels.count){
                self.downloadChapterContent(index+1, progressCall: progressCall)
            }
        })
    }
    
    func downloadOneChapterContent(readChapterListModel:DZMReadChapterListModel,finishCallback:@escaping ((_ success:Bool)->Void)) -> Void {
        if readChapterListModel.isDownload == 0 {
            XSNetWorking.loadChapterContent(readChapterListModel.chapterLink, callback: { (content) in
                if(content != "error"){
                    _ = DZMReadParser.generalChapterModel(self.bookID, content!, GetReadIsTempCache(), readChapterListModel, self.readChapterListModels.count)
                    finishCallback(true);
                }else{
                    finishCallback(false);
                }
            });
        }else{
            finishCallback(true);
        }
    }
    
    /// 修改阅读记录为 指定章节ID 指定页码
    func modifyReadRecordModel(chapterID:String, page:NSInteger = 0, isUpdateFont:Bool = false, isSave:Bool = false) {
        
        readRecordModel.modify(chapterID: chapterID, toPage: page, isUpdateFont: isUpdateFont, isSave: isSave)
    }
    
    /// 修改阅读记录到书签模型
    func modifyReadRecordModel(readMarkModel:DZMReadMarkModel, isUpdateFont:Bool = false, isSave:Bool = false) {
        
        readRecordModel.modify(readMarkModel: readMarkModel, isUpdateFont: isUpdateFont, isSave: isSave)
    }
    
    /// 保存
    func save() {
        
        // 阅读模型
        if GetReadIsTempCache() {
            ReadTmpKeyedArchiver(bookID, fileName: bookID, object: self)
        }else{
            ReadKeyedArchiver(bookID, fileName: bookID, object: self)
        }
        
        // 阅读记录
        readRecordModel.save()
    }
    
    /// 是否存在阅读模型
    class func IsExistReadModel(bookID:String) ->Bool {
        if GetReadIsTempCache() {
            return ReadTmpKeyedIsExistArchiver(bookID, fileName: bookID)
        }else{
            return ReadKeyedIsExistArchiver(bookID, fileName: bookID)
        }
    }
    
    /// 通过ID获得章节列表模型
    func GetReadChapterListModel(chapterID:String) ->DZMReadChapterListModel? {
        
        for model in readChapterListModels {
            
            if model.id == chapterID {
                
                return model
            }
        }
        
        return nil
    }
    
    // MARK: -- 操作 - 书签
    
    /// 添加书签 默认使用当前阅读记录作为书签
    func addMark(readRecordModel:DZMReadRecordModel? = nil) {
        
        let readRecordModel = (readRecordModel != nil ? readRecordModel : self.readRecordModel)!
        
        let readMarkModel = DZMReadMarkModel()
        
        readMarkModel.bookID = readRecordModel.readChapterModel!.bookID
        
        readMarkModel.id = readRecordModel.readChapterModel!.id
        
        readMarkModel.name = readRecordModel.readChapterModel!.name
        
        readMarkModel.location = NSNumber(value: readRecordModel.readChapterModel!.location(page: readRecordModel.page.intValue))
        
        readMarkModel.content = readRecordModel.readChapterModel!.string(page: readRecordModel.page.intValue)
        
        readMarkModel.time = Date()
        
        readMarkModels.append(readMarkModel)
        
        save()
    }
    
    /// 删除书签 默认使用当前存在的书签
    func removeMark(readMarkModel:DZMReadMarkModel? = nil, index:NSInteger? = nil) ->Bool {
        
        if index != nil {
           
            readMarkModels.remove(at: index!)
            
            save()
            
            return true
            
        }else{
            
            let readMarkModel = (readMarkModel != nil ? readMarkModel : self.readMarkModel)
            
            if readMarkModel != nil && readMarkModels.contains(readMarkModel!) {
                
                readMarkModels.remove(at: readMarkModels.index(of: readMarkModel!)!)
                
                save()
                
                return true
            }
        }
        
        return false
    }
    
    /// 检查当前页面是否存在书签 默认使用当前阅读记录作为检查对象
    func checkMark(readRecordModel:DZMReadRecordModel? = nil) ->Bool {
        
        let readRecordModel = (readRecordModel != nil ? readRecordModel : self.readRecordModel)!
        
        let chapterID = readRecordModel.readChapterModel!.id
        
        var results:[DZMReadMarkModel] = []
        
        for model in readMarkModels {
            
            if model.id == chapterID {
                
                results.append(model)
            }
        }
        
        if !results.isEmpty {
            
            // 当前显示页面的Range
            let range = readRecordModel.readChapterModel!.rangeArray[readRecordModel.page.intValue]
            
            // 便利
            for readMarkModel in results {
                
                let location = readMarkModel.location.intValue
                
                if location >= range.location && location < (range.location + range.length) {
                    
                    self.readMarkModel = readMarkModel
                    
                    return true
                }
            }
        }
        
        // 清空
        readMarkModel = nil
        
        return false
    }
    
    // MARK: -- NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init()
        
        bookID = aDecoder.decodeObject(forKey: "bookID") as! String
        
        isLocalBook = aDecoder.decodeObject(forKey: "isLocalBook") as! NSNumber
        
        readChapterListModels = aDecoder.decodeObject(forKey: "readChapterListModels") as! [DZMReadChapterListModel]
        
        readMarkModels = aDecoder.decodeObject(forKey: "readMarkModels") as! [DZMReadMarkModel]
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(bookID, forKey: "bookID")
        
        aCoder.encode(isLocalBook, forKey: "isLocalBook")
        
        aCoder.encode(readChapterListModels, forKey: "readChapterListModels")
        
        aCoder.encode(readMarkModels, forKey: "readMarkModels")
    }
}
