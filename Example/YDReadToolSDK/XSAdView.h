//
//  XSAdView.h
//  novels_ebook
//
//  Created by Chen Qian on 2017/4/26.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^WatchVideoForDownload)(BOOL finish);
typedef void(^OnFrameChange)(NSDictionary *dict, id curview);

@interface XSAdView : UIView

@property (nonatomic,strong) NSString *adType;
@property (nonatomic,strong) NSString *adUnitID;
@property (nonatomic,assign) BOOL startLoadAd;
@property (nonatomic, assign) BOOL isAwalyAd;
@property (nonatomic, assign) BOOL isCustomAd;
@property (nonatomic,assign) NSInteger preWidth;
@property (nonatomic,assign) NSInteger preHeight;
@property (nonatomic,assign) NSTimeInterval toGradeTimeInterval;
@property (nonatomic, copy) OnFrameChange onFrameChange;
@property (nonatomic, copy) WatchVideoForDownload watchVideoForDownload;

-(void)showBunnerView;
-(void)encourageToGrade:(void(^)(void))callback;
-(void)showVideoView:(void(^)(BOOL finish))callback;

@end
