//
//  XSAdView.m
//  novels_ebook
//
//  Created by Chen Qian on 2017/4/26.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "XSAdView.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

#import "YDAppDelegate.h"
#import <MBProgressHUD+DZM.h>

@interface XSAdView ()<GADBannerViewDelegate,GADRewardBasedVideoAdDelegate>

@property (nonatomic, strong) UIView *nativeAdView;
@property (nonatomic, strong) GADAdLoader *adLoader;
@property (nonatomic, strong) UIButton *closeAdBtn;

@end

@implementation XSAdView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setAdView:(UIView *)view {
  // Remove previous ad view.
  [self.nativeAdView removeFromSuperview];
  self.nativeAdView = nil;
  self.nativeAdView = view;
  
  // Add new ad view and set constraints to fill its container.
  view.alpha = 0;
  [self addSubview:_nativeAdView];
  [UIView animateWithDuration:1.30 animations:^{
    view.alpha = 1;
  }];
}

- (void)loadAds{
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_adUnitID) {
      if ([_adType isEqualToString:@"banner"]){
          GADAdSize size = kGADAdSizeSmartBannerPortrait;
          if (_isCustomAd) {
              size = GADAdSizeFromCGSize(CGSizeMake(self.frame.size.width - 32, _preHeight));
          }
//        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:size];
        bannerView.adUnitID = self.adUnitID;
        bannerView.delegate = self;
        bannerView.rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        GADRequest *request = [GADRequest request];
        request.testDevices = @[ kGADSimulatorID ];
        [bannerView loadRequest:request];
        [self setAdView:bannerView];
      }else{
        self.adLoader = [[GADAdLoader alloc] initWithAdUnitID:_adUnitID
                                           rootViewController:[UIApplication sharedApplication].keyWindow.rootViewController
                                                      adTypes:@[kGADAdLoaderAdTypeNativeAppInstall,kGADAdLoaderAdTypeNativeContent]
                                                      options:@[]];
        self.adLoader.delegate = self;
        GADRequest *request = [GADRequest request];
        request.testDevices = @[ kGADSimulatorID ];
        [self.adLoader loadRequest:request];
      }
    }
  });
}

- (void)reactSetFrame:(CGRect)frame
{
  CGSize oldSize = self.bounds.size;
  if (!CGSizeEqualToSize(self.bounds.size, oldSize)) {
    [self.layer setNeedsDisplay];
  }
}

-(void)setHidden:(BOOL)hidden{
  [super setHidden:hidden];
  if (hidden) {
    [self.nativeAdView removeFromSuperview];
  }else{
    self.nativeAdView.frame = self.bounds;
    [self loadAds];
  }
}

-(void)setStartLoadAd:(BOOL)startLoadAd{
  _startLoadAd = startLoadAd;
  if (startLoadAd) {
    self.nativeAdView.frame = CGRectMake(0, 0, _preWidth, _preHeight);
    [self loadAds];
  }
}

- (UIViewController *)reactViewController
{
    id responder = [self nextResponder];
    while (responder) {
        if ([responder isKindOfClass:[UIViewController class]]) {
            return responder;
        }
        responder = [responder nextResponder];
    }
    return nil;
}

#pragma mark - GADNativeAppInstallAdLoaderDelegate, GADNativeContentAdLoaderDelegate,GADVideoControllerDelegate

-(void)adViewDidReceiveAd:(GADBannerView *)bannerView{
  [self setAdView:bannerView];
  if (self.onFrameChange) {
    NSInteger preHeight = ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)?90:50;
    self.onFrameChange(@{@"width":@(_preWidth),@"height":@(preHeight)},self);
  }
  if (self.isAwalyAd && self.closeAdBtn == nil) {
    self.closeAdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_closeAdBtn setFrame:CGRectMake(self.bounds.size.width-30, 0, 30, 30)];
    [_closeAdBtn setImage:[UIImage imageNamed:@"icon_close_ad"] forState:UIControlStateNormal];
    [_closeAdBtn addTarget:self action:@selector(actionSheetView) forControlEvents:UIControlEventTouchUpInside];
    [_nativeAdView addSubview:_closeAdBtn];
  }
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{

}

-(void)adLoader:(GADAdLoader *)adLoader didFailToReceiveAdWithError:(GADRequestError *)error{
  
}

#pragma mark - GADRewardBasedVideoAdDelegate

-(void)actionSheetView{
  UIViewController *controller = [self reactViewController];
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"您可以选择一种方式去除底部广告条" preferredStyle:UIAlertControllerStyleActionSheet];
  [alertController addAction:[UIAlertAction actionWithTitle:@"我要看视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [self showRewardAd];
  }]];
  NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
  BOOL alreadyComment = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"comment_%@",version]];
  BOOL openAdGrade = [[NSUserDefaults standardUserDefaults] boolForKey:@"openAdGrade"];
  if (!alreadyComment && openAdGrade) {
    [alertController addAction:[UIAlertAction actionWithTitle:@"去5星好评(永久去除)" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      [self togoGrade];
    }]];
  }
  [alertController addAction:[UIAlertAction actionWithTitle:@"什么也不做" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    
  }]];
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
    
  }else {
    // Change Rect to position Popover
    alertController.popoverPresentationController.sourceView = _closeAdBtn;
  }
  [controller presentViewController:alertController animated:YES completion:nil];
}

-(void)showBunnerView{
  self.nativeAdView.frame = self.bounds;
  [self loadAds];
}

-(void)loadRewardAd:(NSString*)unitID{
  GADRequest *request = [GADRequest request];
  [GADRewardBasedVideoAd sharedInstance].delegate = self;
  [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                         withAdUnitID:unitID];
}

-(void)showRewardAd{
  if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:[self reactViewController]];
  }else{
    [MBProgressHUD hideHUD];
    [MBProgressHUD showMessage:@"正在请求数据,请稍后..."];
    [self loadRewardAd:@"ca-app-pub-7418367853869463/6541480289"];
  }
}

-(void)showVideoView:(void (^)(BOOL))callback{
  self.watchVideoForDownload = callback;
  if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:[self reactViewController]];
  }else{
    [MBProgressHUD hideHUD];
    [MBProgressHUD showMessage:@"正在请求数据,请稍后..."];
    [self loadRewardAd:@"ca-app-pub-7418367853869463/2197134492"];
  }
}

- (void)toGradeTime{
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
  NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
  if (self.toGradeTimeInterval > 0 && (currentTime - self.toGradeTimeInterval) > 5) {
    [_nativeAdView removeFromSuperview];
    [self removeFromSuperview];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:[NSString stringWithFormat:@"comment_%@",version]];
    [[NSUserDefaults standardUserDefaults] setDouble:_toGradeTimeInterval forKey:@"RADTIMEFORPAY"];
    [[NSUserDefaults standardUserDefaults] setInteger:900 forKey:@"RADTIMELONG"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"RADINVILD"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"comment_once"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notif_remove_ad" object:@(true)];
  }
  self.toGradeTimeInterval = 0;
}

- (void)togoGrade{
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toGradeTime) name:UIApplicationWillEnterForegroundNotification object:nil];
    YDAppDelegate* appdelegate = (YDAppDelegate*)[UIApplication sharedApplication].delegate;
    appdelegate.gadInterstitialIsClick = true;
  NSString *version = [UIDevice currentDevice].systemVersion;
  if (version.doubleValue >= 11.0) {
      [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/id1365360383?mt=8&action=write-review"]];
  } else {
      [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1365360383&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"]];
  }
  self.toGradeTimeInterval = [[NSDate date] timeIntervalSince1970];

}

- (void)encourageToGrade:(void (^)(void))callback{
    BOOL isAlreadyGrade = [[NSUserDefaults standardUserDefaults] boolForKey:@"comment_once"];
    double currentTime = [[NSDate date] timeIntervalSince1970];
    double preAlertCommentTime = [[NSUserDefaults standardUserDefaults] doubleForKey:@"preAlertCommentTime"];
    if (!isAlreadyGrade && (currentTime - preAlertCommentTime > 72*3600)) {
        double userALongTime = [[NSUserDefaults standardUserDefaults] doubleForKey:@"appFirstLaunchTime"];
        BOOL un_comment_once = ((currentTime - userALongTime) > 120*3600);
        NSString *titleStr = @"好评获取VIP书源,并永久去除阅读页面底部广告";
        NSString *messageStr = @"评论内容已复制，点击确定，写上标题，直接粘贴即可。";
        if (un_comment_once) {
            titleStr = @"";
            messageStr = @"用的怎么样,有什么想说的吗?如果还行,就为我点个赞吧";
        }
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSArray *comments = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"comments" ofType:@"plist"]];
        if (comments && comments.count > 0) {
            pasteboard.string = comments[arc4random()%comments.count];
        }
        UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleStr message:messageStr preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"5星好评" style:2 handler:^(UIAlertAction * _Nonnull action) {
            [self togoGrade];
        }]];
        if (un_comment_once) {
            [alertController addAction:[UIAlertAction actionWithTitle:@"我要吐槽" style:0 handler:^(UIAlertAction * _Nonnull action) {
                [[NSUserDefaults standardUserDefaults] setDouble:currentTime forKey:@"preAlertCommentTime"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self togoGrade];
            }]];
        }
        [alertController addAction:[UIAlertAction actionWithTitle:@"再看会儿" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (un_comment_once) {
                [[NSUserDefaults standardUserDefaults] setDouble:currentTime forKey:@"preAlertCommentTime"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }]];
        [controller presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
  if ([reward.type isEqualToString:@"removeBunder"]) {
    [_nativeAdView removeFromSuperview];
    [self removeFromSuperview];
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"RADTIMEFORPAY"];
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"RADTIMELONG"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"RADINVILD"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notif_remove_ad" object:@(true)];
  }else{
    if (_watchVideoForDownload) {
      self.watchVideoForDownload(true);
    }
  }
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
  [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:[self reactViewController]];
  [MBProgressHUD hideHUD];
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error{
  [MBProgressHUD hideHUD];
  [MBProgressHUD showHitMessage:error.description];
  if (_watchVideoForDownload) {
    self.watchVideoForDownload(false);
  }
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
  [MBProgressHUD hideHUD];
  if (_watchVideoForDownload) {
    self.watchVideoForDownload(false);
  }
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
  [MBProgressHUD hideHUD];
//  if (_watchVideoForDownload) {
//    self.watchVideoForDownload(rewardBasedVideoAd.isReady);
//  }
}

@end
