//
//  XSListenController.h
//  NovelRead
//
//  Created by Chen Qian on 2018/3/25.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DZMReadController;
@interface XSListenController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *bookCorverImage;
@property (nonatomic, weak) IBOutlet UISlider *bookSlider;
@property (nonatomic, weak) IBOutlet UISlider *rateSlider;
@property (nonatomic, weak) IBOutlet UISlider *pitchMultiplierSlider;
@property (nonatomic, weak) IBOutlet UILabel *leftPageLabel;
@property (nonatomic, weak) IBOutlet UILabel *rightPageLabel;
@property (nonatomic, weak) IBOutlet UIButton *bookPlayBtn;
@property (nonatomic, weak) IBOutlet UIButton *prePlayBtn;
@property (nonatomic, weak) IBOutlet UIButton *nextPlayBtn;
@property (nonatomic, weak) IBOutlet UIButton *pthBtn;
@property (nonatomic, weak) IBOutlet UIButton *yyBtn;
@property (nonatomic, weak) IBOutlet UIButton *twBtn;
@property (nonatomic, assign) BOOL isListening;
@property (nonatomic, copy) NSString *currentListenString;
@property (nonatomic, weak) DZMReadController *vcController;
@property (nonatomic, assign) NSInteger currentPageCount;
@property (nonatomic, assign) NSInteger currentPageIndex;
@property (nonatomic, assign) NSInteger currentChapterIdNum;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageTop;

-(void)playWithString:(NSString*)string;

@end
