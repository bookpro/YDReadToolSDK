//
//  XSListenController.m
//  NovelRead
//
//  Created by Chen Qian on 2018/3/25.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import "XSListenController.h"
#import "YDReadToolSDK_Example-Swift.h"
#import <XSVoiceSynthesizer.h>
#import <XSConfig.h>
#import <UIImageView+AFNetworking.h>
#import <MediaPlayer/MediaPlayer.h>

@interface XSListenController ()

@end

@implementation XSListenController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoToNextPage) name:@"auto_to_next_page" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backContine) name:@"back_play_contine" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPause) name:@"back_play_pause" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backStop) name:@"back_play_stop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nextPlayBtnClick:) name:@"back_play_next" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prePlayBtnClick:) name:@"back_play_pre" object:nil];
    
    
    self.bookPlayBtn.selected = YES;
    self.rightPageLabel.text = [NSString stringWithFormat:@"共%ld页",self.currentPageCount];
    self.leftPageLabel.text = [NSString stringWithFormat:@"第%ld页",self.currentPageIndex+1];
    self.bookSlider.minimumValue = 0;
    self.bookSlider.maximumValue = _currentPageCount - 1;
    self.rateSlider.maximumValue = AVSpeechUtteranceMaximumSpeechRate;
    self.rateSlider.minimumValue = AVSpeechUtteranceMinimumSpeechRate;
    self.pitchMultiplierSlider.maximumValue = 2.0;
    self.pitchMultiplierSlider.minimumValue = 0.5;
    [self.bookSlider setValue:_currentPageIndex animated:YES];
    [self.rateSlider setValue:[XSVoiceSynthesizer shareManager].rate animated:YES];
    [self.pitchMultiplierSlider setValue:[XSVoiceSynthesizer shareManager].pitchMultiplier animated:YES];
    
    if ([UIScreen mainScreen].bounds.size.height == 812) {
        self.iconImageTop.constant = 150;
    }else {
        self.iconImageTop.constant = 90;
    }
    
    __weak typeof(self) tpself = self;
    [self.bookCorverImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://statics.zhuishushenqi.com%@",_vcController.currentBookCoverImageUrl]]] placeholderImage:[UIImage imageNamed:@"icon_64"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        tpself.bookCorverImage.image = image;
        [tpself setBackDisplayContent];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //设置并激活音频会话类别
    
    AVAudioSession *session=[AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    [session setActive:YES error:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.isListening = false;
    self.currentListenString = nil;
    [[XSVoiceSynthesizer shareManager] stop];
    AVAudioSession *session=[AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:NO error:nil];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backContine{
    [[XSVoiceSynthesizer shareManager] contine];
}
-(void)backPause{
    [[XSVoiceSynthesizer shareManager] pause];
}
-(void)backStop{
    [[XSVoiceSynthesizer shareManager] stop];
}

-(void)setBackDisplayContent{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
    [dic setObject:self.title forKey:MPMediaItemPropertyTitle];
    if (_vcController.currentBookName) {
        [dic setObject:_vcController.currentBookName forKey:MPMediaItemPropertyAlbumTitle];
    }
    if (_vcController.currentBookAuthor) {
        [dic setObject:_vcController.currentBookAuthor forKey:MPMediaItemPropertyArtist];
    }
    if (_bookCorverImage.image) {
        MPMediaItemArtwork *artWork = [[MPMediaItemArtwork alloc] initWithImage:_bookCorverImage.image];
        [dic setObject:artWork forKey:MPMediaItemPropertyArtwork];
    }
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:dic];
}

-(void)playWithString:(NSString *)string{
    if (string && string.length > 100 && _currentListenString && _currentListenString.length > 100) {
        if ([[string substringToIndex:100] isEqualToString:[_currentListenString substringToIndex:100]]) {
            return;
        }
    }
    self.bookPlayBtn.selected = YES;
    self.currentListenString = string;
    self.rightPageLabel.text = [NSString stringWithFormat:@"共%ld页",self.currentPageCount];
    self.leftPageLabel.text = [NSString stringWithFormat:@"第%ld页",self.currentPageIndex+1];
    self.bookSlider.maximumValue = _currentPageCount - 1;
    [self.bookSlider setValue:self.currentPageIndex animated:YES];
    [[XSVoiceSynthesizer shareManager] play:string];
    self.isListening = true;
    [self setBackDisplayContent];
}

-(void)autoToNextPage{
    if (_currentPageIndex == _currentPageCount - 1) {
        self.currentChapterIdNum += 1;
        self.currentPageIndex = 0;
    }else{
        self.currentPageIndex += 1;
    }
    [self.vcController.readOperation GoToChapterWithChapterID:[NSString stringWithFormat:@"%ld",self.currentChapterIdNum] toPage:self.currentPageIndex];
}

-(IBAction)prePlayBtnClick:(id)sender{
    self.currentChapterIdNum -= 1;
    [self.vcController.readOperation GoToChapterWithChapterID:[NSString stringWithFormat:@"%ld",self.currentChapterIdNum] toPage:0];
}
-(IBAction)nextPlayBtnClick:(id)sender{
    self.currentChapterIdNum += 1;
    [self.vcController.readOperation GoToChapterWithChapterID:[NSString stringWithFormat:@"%ld",self.currentChapterIdNum] toPage:0];
}
-(IBAction)bookPlayBtnClick:(UIButton*)sender{
    if ([sender isSelected]) {
        [[XSVoiceSynthesizer shareManager] pause];
    }else{
        [[XSVoiceSynthesizer shareManager] contine];
    }
    sender.selected = !sender.selected;
}
-(IBAction)pthBtnClick:(id)sender{
    [[XSVoiceSynthesizer shareManager] setSelectVoiceIndex:0];
}
-(IBAction)yyBtnClick:(id)sender{
    [[XSVoiceSynthesizer shareManager] setSelectVoiceIndex:1];
}
-(IBAction)twBtnClick:(id)sender{
    [[XSVoiceSynthesizer shareManager] setSelectVoiceIndex:2];
}
-(IBAction)sliderChange:(UISlider*)sender{
    self.currentPageIndex = (NSInteger)sender.value;
    [self.vcController.readOperation GoToChapterWithChapterID:[NSString stringWithFormat:@"%ld",self.currentChapterIdNum] toPage:_currentPageIndex];
}
-(IBAction)rateSliderChange:(UISlider*)sender{
    [[XSVoiceSynthesizer shareManager] setRate:sender.value];
}
-(IBAction)pitchMultiplierSliderChange:(UISlider*)sender{
    [[XSVoiceSynthesizer shareManager] setPitchMultiplier:sender.value];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
