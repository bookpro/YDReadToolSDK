//
//  YDAppDelegate.h
//  YDReadToolSDK
//
//  Created by 善良的快递费 on 03/30/2019.
//  Copyright (c) 2019 善良的快递费. All rights reserved.
//

@import UIKit;

@protocol DZMAppDelegate <NSObject>

@optional
/// 程序即将退出
-(void)applicationWillTerminate:(UIApplication*)application;

/// 内存警告可能要终止程序
-(void)applicationDidReceiveMemoryWarning:(UIApplication*)application;

///切换阅读模式
-(void)applicationDidFinishLaunching:(UIApplication*)application;

@end

@interface YDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) id<DZMAppDelegate> delegate;
@property (nonatomic, assign) BOOL gadInterstitialIsClick;
@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTaskIdentifier;

@end
