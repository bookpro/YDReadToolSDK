//
//  YDViewController.m
//  YDReadToolSDK
//
//  Created by 善良的快递费 on 03/30/2019.
//  Copyright (c) 2019 善良的快递费. All rights reserved.
//

#import "YDViewController.h"
#import "YDReadToolSDK_Example-Swift.h"
#import <MBProgressHUD+DZM.h>

@interface YDViewController ()

@end

@implementation YDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)pushToRead:(id)sender{
    //5712a3eeb28fed3e095c8b0c
    [self loadChapterListData:^(id error, NSString *bookid) {
        if ([error isKindOfClass:[NSString class]]) {
            [MBProgressHUD showError:error];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                DZMReadController *readController = [[DZMReadController alloc] init];
                readController.hidesBottomBarWhenPushed = YES;
                readController.automaticallyAdjustsScrollViewInsets = NO;
                readController.extendedLayoutIncludesOpaqueBars = true;
                readController.readModel = error;
                readController.currentBookCoverImageUrl =@"";
                readController.currentBookAuthor = @"ceshi";
                readController.currentBookName = @"xiaoshuo";
                [self.navigationController pushViewController:readController animated:YES];
            });
        }
    }];
}

-(void)loadChapterListData:(void(^)(id error,NSString *bookid))callback{
    NSString *bookId = @"53e56ee335f79bb626a496c9";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __block DZMReadModel *readModel = [DZMReadModel readModelWithNetBook:bookId :false];
    if (readModel) {
        callback(readModel,bookId);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    [XSNetWorking loadChapterList:bookId originId:nil callback:^(id chaptersList) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if ([chaptersList isKindOfClass:[NSArray class]]) {
            readModel = [DZMReadModel readModelWithNetBook:bookId :false];
            if (readModel) {
                callback(readModel,bookId);
            }else{
                callback(@"小说章节不存在",bookId);
            }
        }else{
            callback(@"小说章节不存在",bookId);
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
