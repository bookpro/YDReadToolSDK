//
//  main.m
//  YDReadToolSDK
//
//  Created by 善良的快递费 on 03/30/2019.
//  Copyright (c) 2019 善良的快递费. All rights reserved.
//

@import UIKit;
#import "YDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YDAppDelegate class]));
    }
}
