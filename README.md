# YDReadToolSDK

[![CI Status](https://img.shields.io/travis/善良的快递费/YDReadToolSDK.svg?style=flat)](https://travis-ci.org/善良的快递费/YDReadToolSDK)
[![Version](https://img.shields.io/cocoapods/v/YDReadToolSDK.svg?style=flat)](https://cocoapods.org/pods/YDReadToolSDK)
[![License](https://img.shields.io/cocoapods/l/YDReadToolSDK.svg?style=flat)](https://cocoapods.org/pods/YDReadToolSDK)
[![Platform](https://img.shields.io/cocoapods/p/YDReadToolSDK.svg?style=flat)](https://cocoapods.org/pods/YDReadToolSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YDReadToolSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YDReadToolSDK'
```

## Author

善良的快递费, 2013_0810@sina.com

## License

YDReadToolSDK is available under the MIT license. See the LICENSE file for more info.
