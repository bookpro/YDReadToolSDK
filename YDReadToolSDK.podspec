#
# Be sure to run `pod lib lint YDReadToolSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YDReadToolSDK'
  s.version          = '2.0.1'
  s.summary          = 'A short description of YDReadToolSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                        gongju 2.0.1
                       DESC

  s.homepage         = 'https://gitee.com/bookpro/YDReadToolSDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '善良的快递费' => '2013_0810@sina.com' }
  s.source           = { :git => 'https://gitee.com/bookpro/YDReadToolSDK.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'YDReadToolSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'YDReadToolSDK' => ['YDReadToolSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.static_framework = true
   s.dependency 'AFNetworking'
end
