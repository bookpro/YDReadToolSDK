//
//  CSTURLHttpRequest.h
//  ctvonline
//
//  Created by QianChen on 14/11/20.
//  Copyright (c) 2014年 ChenQian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#define Event_Path [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"event"]
#define Event_Path_Temp [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"eventTemp"]
///网络状况判断
#define NetWork_IsReachable [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus
///没有网络显示字符
FOUNDATION_EXTERN NSString * const NetWork_NetNoneText;
///请求失败显示字符
FOUNDATION_EXTERN NSString * const NetWork_RequestFailText;

/**
 CSTDownloadStatus 下载状态

 - CST_DOWNLOAD_STATUS_LOADING: 下载中
 - CST_DOWNLOAD_STATUS_FAIL:    下载失败
 - CST_DOWNLOAD_STATUS_PAUSE:   下载暂停
 - CST_DOWNLOAD_STATUS_FINISH:  下载完成
 */
typedef NS_ENUM(NSInteger,CSTDownloadStatus) {
    CST_DOWNLOAD_STATUS_LOADING = 10,
    CST_DOWNLOAD_STATUS_FAIL,
    CST_DOWNLOAD_STATUS_PAUSE,
    CST_DOWNLOAD_STATUS_FINISH
};


/**
 请求和响应返回序列化数据

 - CST_SERIALIZER_REQUEST_RESPONSE_HTTP: request:http,response:http
 - CST_SERIALIZER_REQUEST_RESPONSE_JSON: request:json,response:json
 - CST_SERIALIZER_REQUEST_HTTP_RESPONSE_JSON: request:http,response:json
 - CST_SERIALIZER_REQUEST_JSON_RESPONSE_HTTP: request:json,response:http
 */
typedef NS_ENUM(NSInteger,CSTSerializerType) {
    CST_SERIALIZER_REQUEST_RESPONSE_HTTP,
    CST_SERIALIZER_REQUEST_RESPONSE_JSON,
    CST_SERIALIZER_REQUEST_HTTP_RESPONSE_JSON,
    CST_SERIALIZER_REQUEST_JSON_RESPONSE_HTTP
};

@class CSTURLDownloadRequest;


/**
 只读类！！！app中断或者退出后，缓存在本地的未完成的下载信息类。使用者不需要保存此类信息
 */
@interface CSTDownloadCacheInfo : NSObject<NSCoding>

/**
 唯一标示
 */
@property (nonatomic, copy) NSString *downloadIdentifier;

/**
 断点数据
 */
@property (nonatomic, strong) NSData *downloadResumeData;

/**
 是否后台下载
 */
@property (nonatomic, assign) BOOL downloadIsBackground;

/**
 中断时下载状态
 */
@property (nonatomic, assign) CSTDownloadStatus downloadStatus;

@end


/**
 网络请求类，提供get/post/upload/download等多种网络请求方式，支持后台下载。get/post/upload方法基于afnetworking封装。解决afnetworking内存泄露问题。开发者使用该类来完成所有网络操作。
 */
@interface CSTURLHttpRequest : NSObject
{
    
}

/**
 创建单利类

 @return return CSTURLHttpRequest
 */
+(instancetype)shareManager;
/**
 向服务器发送事件统计
 */
+(void)postEvent;

/**
 设置user-agent
 */
+(void)setUserAgent:(NSString*)userAgent idfa:(NSString*)adfa;
/**
 全局设置-设置请求序列化

 @param SerializerType request response
 */
+(void)setHttpRequestAndResponseSerializer:(CSTSerializerType)SerializerType;
/**
 全局设置-开启网络检测

 @param netStatus 当前网络状态
 */
+(void)startNetMonitoring:(void(^)(AFNetworkReachabilityStatus status))netStatus;
/**
 全局设置-开启SSL请求
 */
+(void)startSSLRequest;

/**
 全局设置-关闭SSL请求
 */
+(void)cancelSSLRequest;
/**
 get   此方法防止AF内存泄露
 
 @param url     url
 @param dict    参数
 @param success 响应成功
 @param fail    响应失败
 */
+(NSURLSessionDataTask*)get:(NSString*)url
                      parms:(NSDictionary*)dict
                    success:(void (^)(id responseObject))success
                       fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 post 此方法防止AF内存泄露
 
 @param url     url
 @param dict    参数
 @param success 响应成功
 @param fail    响应失败
 */
+(NSURLSessionDataTask*)post:(NSString*)url
                       parms:(NSDictionary*)dict
                     success:(void (^)(id responseObject))success
                        fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 以文件路径方式上传单个文件

 @param url      接口url
 @param filePath 文件路径
 @param dict     参数
 @param progress 进度条属性:totalUnitCount上传总大小，completedUnitCount完成总大小，fractionCompleted完成百分比
 @param success  成功
 @param fail     失败

 @return NSURLSessionUploadTask
 */
+(NSURLSessionUploadTask*)uploadUseFile:(NSString*)url
                               filePath:(NSString*)filePath
                                 params:(NSDictionary*)dict
                               progress:(void(^)(NSProgress *uploadProgress))progress
                                success:(void(^)(id responseObject))success
                                   fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 以文件路径方式上传多个文件
 
 @param url       接口url
 @param filesPath 文件路径，数组形式
 @param dict      参数
 @param progress  进度条属性:totalUnitCount上传总大小，completedUnitCount完成总大小，fractionCompleted完成百分比
 @param success   成功
 @param fail      失败
 
 @return NSURLSessionUploadTask
 */
+(NSURLSessionUploadTask*)uploadUseFiles:(NSString*)url
                                filePath:(NSArray*)filesPath
                                  params:(NSDictionary*)dict
                                progress:(void(^)(NSProgress *uploadProgress))progress
                                 success:(void(^)(id responseObject))success
                                    fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 以二进制方式上传单个文件
 
 @param url      接口url
 @param info     包含参数：data、filename、mimeType   注意区分大小写
 infos = @{data:二进制数据,filename:@"二进制文件名",mimeType:@"image/png"};
 @param dict     参数
 @param progress 进度条属性:totalUnitCount上传总大小，completedUnitCount完成总大小，fractionCompleted完成百分比
 @param success  成功
 @param fail     失败
 
 @return NSURLSessionUploadTask
 */
+(NSURLSessionUploadTask*)uploadUseData:(NSString*)url
                               dataInfo:(NSDictionary*)info
                                 params:(NSDictionary*)dict
                               progress:(void(^)(NSProgress *uploadProgress))progress
                                success:(void(^)(id responseObject))success
                                   fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 以二进制方式上传多个文件

 @param url       接口url
 @param infos     要上传的二进制名称和对应的mimeType，以及data数据，默认mimeType=application/octet-stream,示例：
                  包含参数：data、filename、mimeType   注意区分大小写
                  infos = @[{data:二进制数据,filename:@"二进制文件名",mimeType:@"image/png"}];
 @param dict      参数
 @param progress  进度条属性:totalUnitCount上传总大小，completedUnitCount完成总大小，fractionCompleted完成百分比
 @param success   成功
 @param fail      失败

 @return NSURLSessionUploadTask
 */
+(NSURLSessionUploadTask*)uploadUseDatas:(NSString*)url
                               dataInfos:(NSArray*)infos
                                  params:(NSDictionary*)dict
                                progress:(void(^)(NSProgress *uploadProgress))progress
                                 success:(void(^)(id responseObject))success
                                    fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 下载文件

 @param url                  服务器url
 @param identifier           唯一标识符，可以为nil
 @param isBackgroundDownload 是否支持后台下载
 @param progress             下载进度
 @param success              下载成功
 @param downloadPause        下载暂停
 @param fail                 下载失败
 */
+(void)downloadData:(NSString*)url
                              identifier:(NSString*)identifier
                    isBackgroundDownload:(BOOL)isBackgroundDownload
                                progress:(void(^)(double percent))progress
                                 success:(void(^)(id error,NSURL *originUrl,NSURL *destURL))success
                           downloadPause:(void(^)(id partialData))downloadPause
                                    fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;

/**
 断点继续下载

 @param resumeData           上次下载到一半获取到的resumeData数据,永远新建请求下载
 @param isBackgroundDownload 是否支持后台
 @param progress             下载进度
 @param success              下载成功
 @param downloadPause        下载暂停
 @param fail                 下载失败
 */
+(void)resumeDownloadData:(NSData*)resumeData
                          isBackgroundDownload:(BOOL)isBackgroundDownload
                                      progress:(void(^)(double percent))progress
                                       success:(void(^)(id error,NSURL *originUrl,NSURL *destURL))success
                                 downloadPause:(void(^)(id partialData))downloadPause
                                          fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 断点继续下载
 
 @param identifier           上次下载到一半获取到的identifier数据，获取缓存请求下载
 @param isBackgroundDownload 是否支持后台
 @param progress             下载进度
 @param success              下载成功
 @param downloadPause        下载暂停
 @param fail                 下载失败
 */
+(void)resumeDownloadIdentifier:(NSString*)identifier
                          isBackgroundDownload:(BOOL)isBackgroundDownload
                                      progress:(void(^)(double percent))progress
                                       success:(void(^)(id error,NSURL *originUrl,NSURL *destURL))success
                                 downloadPause:(void(^)(id partialData))downloadPause
                                          fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
/**
 后台下载完成后回调设置
 
 @param identifier 后台session唯一标示
 */
+(void)setBackgroundSessionCompletionHandlerWithIdentifier:(NSString*)identifier completionHandler:(void (^)())completionHandler;
/**
 获取本地缓存的下载任务信息，读取一次后自动删除identifier对应的信息对象，该对象用来初始化上次退出app后残留的后台下载任务

 @param identifier 下载任务唯一标识符

 @return CSTDownloadCacheInfo
 */
+(CSTDownloadCacheInfo*)downloadInfoWithIdentifier:(NSString*)identifier;
/**
 暂停下载任务

 @param identifier 唯一标示
 @param resumeDataBlock 返回已下载数据，作为断点下载使用，数据也可用从类方法downloadData的downloadPause回调中获取
 */
+(void)pauseDownload:(NSString*)identifier resumeDataBlock:(void(^)(NSData *resumeData))resumeDataBlock;

/**
 关闭下载任务

 @param identifier 唯一标示
 */
+(void)cancelDownload:(NSString*)identifier;

/**
 关闭所有下载
 */
+(void)cancelAllDownloads;
/**
 关闭请求任务

 @param dataTask dataTask
 */
+(void)cancleRequest:(NSURLSessionDataTask*)dataTask;
/**
 关闭所有请求
 */
+(void)cancleAllRequest;

@end
