//
//  CSTURLHttpRequest.m
//  ctvonline
//
//  Created by QianChen on 14/11/20.
//  Copyright (c) 2014年 ChenQian. All rights reserved.
//

#import "CSTURLHttpRequest.h"

#if __has_include("JSONKit.h")

#import "JSONKit.h"

#endif

#if __has_include("NSData+CSTGzip.h")

#import "NSData+CSTGzip.h"

#endif

#define DownloadMaximumConnectionsPerHost 5

NSString * const NetWork_NetNoneText = @"无法检测到网络!";
NSString * const NetWork_RequestFailText = @"无法请求到数据!";

static NSString *cst_download_type_background_resume = @"cst_download_type_background_resume";
static NSString *cst_download_type_background_general = @"cst_download_type_background_general";
static NSString *cst_download_type_normal_resume = @"cst_download_type_normal_resume";
static NSString *cst_download_type_normal_general = @"cst_download_type_normal_general";
static NSString *cst_download_type_background = @"cst_download_type_background";
static NSString *cst_download_type_normal = @"cst_download_type_normal";
static NSString *cst_download_resume_tempfile_suffix = @".temp";
static NSString *cst_download_crash_notifcation = @"cst_download_crash_notifcation";
static NSString *cst_download_cache_file_download_requests = @"cstDownloadRequests";
static NSString *cst_download_cache_file_request_data = @"cstRequestData";

/**
 得到随机名字

 @param lastComponent 根据lastComponent生成随机数

 @return NSString
 */
static NSString* getRandomNameFor(NSString* lastComponent){
    if (!lastComponent || [lastComponent rangeOfString:@"."].location == NSNotFound) {
        return [[NSProcessInfo processInfo] globallyUniqueString];
    }else{
        NSRange range = [lastComponent rangeOfString:@"."];
        NSString *timestr = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]] stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSString *filename = [NSString stringWithFormat:@"%@_%@",[lastComponent substringToIndex:MAX(range.location, 0)],timestr];
        filename = [NSString stringWithFormat:@"%@%@",filename,[lastComponent substringFromIndex:range.location]];
        return filename;
    }
}

/**
 下载队列
 
 @return dispatch_queue_t
 */
static dispatch_queue_t url_session_manager_download_queue() {
    static dispatch_queue_t cst_url_session_manager_download_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cst_url_session_manager_download_queue = dispatch_queue_create("download.sesition.serial.queue", DISPATCH_QUEUE_SERIAL);
    });
    return cst_url_session_manager_download_queue;
}

/**
 创建下载后数据保存路径

 @return 文件URL
 */
static NSURL* createTempFileurlForCSTDownload(){
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *URLs = [fileManager URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = URLs[0];
    NSURL *tempurl = [documentsDirectory URLByAppendingPathComponent:@"CSTDownload"];
    if (![fileManager fileExistsAtPath:tempurl.absoluteString]) {
        [fileManager createDirectoryAtURL:tempurl withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return tempurl;
}


/**
 异常捕获

 @param exception 异常信息
 */
static void CST_ExceptionCaptureHander(NSException *exception){
    
    [[NSNotificationCenter defaultCenter] postNotificationName:cst_download_crash_notifcation object:nil];
    
}


/**
 私有类！！下载请求类，所有下载核心代码通过该类实现，不要自行调用该类的任何方法。
 */
@interface CSTURLDownloadRequest : NSObject<NSURLSessionDownloadDelegate>


/**
 下载对应的session
 */
@property (nonatomic, strong) NSURLSession *downloadSession;

/**
 当前下载任务
 */
@property (nonatomic, strong) NSURLSessionDownloadTask *donloadTask;

/**
 任务完成后把自己从CSTURLHttpRequest管理类中移除
 */
@property (nonatomic, copy) void(^removeSelfFromArray)(id refSelf);
@property (nonatomic, copy) void(^downloadSuccuss)(id error,NSURL *originUrl,NSURL *destUrl);
@property (nonatomic, copy) void(^downloadFail)(id error,AFNetworkReachabilityStatus reachable);
@property (nonatomic, copy) void(^downloadProgress)(double percent);
@property (nonatomic, copy) void(^downloadPause)(id partialData);
@property (nonatomic, copy) void(^backgroundSessionCompletionHandler)();

/**
 下载唯一标识符
 */
@property (nonatomic, copy, readonly) NSString *downloadIdentifier;

/**
 下载状态
 */
@property (nonatomic, assign, readonly) CSTDownloadStatus downloadStatus;

/**
 是否支持后台下载
 */
@property (nonatomic, assign, readonly) BOOL isBackgroundDownload;

/**
 是否正常下载，还是通过Identifier或者resumeData进行的断点下载
 */
@property (nonatomic, assign, readonly) BOOL isNormalDownload;

/**
 断点下载数据
 */
@property (nonatomic, strong, readonly) NSData *downloadResumeData;

-(NSURLSessionDownloadTask*)downloadData:(NSString*)url
                              identifier:(NSString*)identifier
                    isBackgroundDownload:(BOOL)isBackgroundDownload
                                progress:(void(^)(double percent))progress
                                 success:(void(^)(id error,NSURL *originUrl,NSURL *destURL))success
                           downloadPause:(void(^)(id partialData))downloadPause
                                    fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;

-(NSURLSessionDownloadTask*)resumeDownloadData:(NSString*)url
                                    identifier:(NSString*)identifier
                          isBackgroundDownload:(BOOL)isBackgroundDownload
                                    resumeData:(NSData*)resumeData
                                      progress:(void(^)(double percent))progress
                                       success:(void(^)(id error,NSURL *originUrl,NSURL *destURL))success
                                 downloadPause:(void(^)(id partialData))downloadPause
                                          fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail;
-(void)pauseDownloadTask:(void(^)(NSData *resumeData))resumeDataBlock;
-(void)cancelDownloadTask;

@end

@implementation CSTURLDownloadRequest

-(instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(instancetype)initWithIdentifier:(NSString*)identifier isBackgroundDownload:(BOOL)isBackgroundDownload resumeData:(NSData*)resumeData{
    self = [super init];
    if (self) {
        _downloadStatus = CST_DOWNLOAD_STATUS_PAUSE;
        _downloadIdentifier = identifier;
        _downloadResumeData = resumeData;
        if (isBackgroundDownload) {
            _isBackgroundDownload = YES;
        }else{
            _isNormalDownload = YES;
        }
    }
    return self;
}

- (NSURLSession *)backgroundSession {
    if (!_downloadSession) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[cst_download_type_background stringByAppendingString:_downloadIdentifier]];
        _downloadSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return _downloadSession;
}


- (NSURLSession *)normalSession {
    if (!_downloadSession) {
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        _downloadSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
        _downloadSession.sessionDescription = cst_download_type_normal;
    }
    return _downloadSession;
}

- (NSURLSessionDownloadTask*)startBackgroundDownload:(NSString*)url resumeData:(NSData*)resumeData {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLSession *backgroundSession = [self backgroundSession];
    
    __block NSError *error = nil;
    __weak typeof(self) tpself = self;
    dispatch_sync(url_session_manager_download_queue(), ^{
        if (_isNormalDownload) {
            __strong typeof(tpself) strongSelf = tpself;
            [_downloadSession getAllTasksWithCompletionHandler:^(NSArray<__kindof NSURLSessionTask *> * _Nonnull tasks) {
                if (tasks.count <= 0) {
                    strongSelf.donloadTask = [backgroundSession downloadTaskWithRequest:request];
                    strongSelf.donloadTask.taskDescription = [cst_download_type_background_general stringByAppendingString:strongSelf.downloadIdentifier];
                }else{
                    for (id task in tasks) {
                        if ([task isKindOfClass:[NSURLSessionDownloadTask class]]) {
                            strongSelf.donloadTask = task;
                            break;
                        }
                    }
                }
                if (strongSelf.donloadTask && [strongSelf.donloadTask state] != NSURLSessionTaskStateRunning) {
                    [strongSelf.donloadTask resume];
                }
            }];
        }else{
            if (resumeData) {
                NSData *newResumeData = resumeData;
                if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
                    newResumeData = [self correctResumeData:resumeData];
                }
                [self moveFileToTempForResumeDownload:newResumeData];
                _donloadTask = [backgroundSession downloadTaskWithResumeData:newResumeData];
                _donloadTask.taskDescription = [cst_download_type_background_resume stringByAppendingString:_downloadIdentifier];
                [_donloadTask resume];
            }else{
                _downloadStatus = CST_DOWNLOAD_STATUS_LOADING;
                __strong typeof(tpself) strongSelf = tpself;
                [_downloadSession getAllTasksWithCompletionHandler:^(NSArray<__kindof NSURLSessionTask *> * _Nonnull tasks) {
                    if (tasks.count > 0) {
                        for (id task in tasks) {
                            if ([task isKindOfClass:[NSURLSessionDownloadTask class]]) {
                                strongSelf.donloadTask = task;
                                return ;
                            }
                        }
                    }else{
                        _downloadStatus = CST_DOWNLOAD_STATUS_FAIL;
                        strongSelf.removeSelfFromArray(strongSelf);
                        strongSelf.downloadFail([NSError errorWithDomain:@"没有找到下载任务，请重新开始" code:10000505 userInfo:nil],NetWork_IsReachable);
                    }
                }];
            }
        }
        if (error) {
            _downloadStatus = CST_DOWNLOAD_STATUS_FAIL;
            _downloadFail(error,NetWork_IsReachable);
            _removeSelfFromArray(self);
        }else{
            _downloadStatus = CST_DOWNLOAD_STATUS_LOADING;
        }
    });
    return _donloadTask;
}

- (NSURLSessionDownloadTask*)startNormalDownload:(NSString*)url  resumeData:(NSData*)resumeData {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLSession *normalSession = [self normalSession];
    __block NSError *error = nil;
    dispatch_sync(url_session_manager_download_queue(), ^{
        if (_isNormalDownload) {
            _donloadTask = [normalSession downloadTaskWithRequest:request];
            _donloadTask.taskDescription = [cst_download_type_normal_general stringByAppendingString:_downloadIdentifier];
        }else{
            if (resumeData) {
                [self moveFileToTempForResumeDownload:resumeData];
                _donloadTask = [normalSession downloadTaskWithResumeData:resumeData];
                _donloadTask.taskDescription = [cst_download_type_normal_resume stringByAppendingString:_downloadIdentifier];
            }else{
                error = [NSError errorWithDomain:@"普通断点下载失败，没有resumeData数据" code:10000501 userInfo:nil];
            }
        }
        if (error) {
            _downloadStatus = CST_DOWNLOAD_STATUS_FAIL;
            _downloadFail(error,NetWork_IsReachable);
            _removeSelfFromArray(self);
        }else{
            _downloadStatus = CST_DOWNLOAD_STATUS_LOADING;
            [_donloadTask resume];
        }
    });
    return _donloadTask;
}

-(NSURLSessionDownloadTask*)downloadData:(NSString *)url identifier:(NSString *)identifier isBackgroundDownload:(BOOL)isBackgroundDownload progress:(void (^)(double))progress success:(void (^)(id, NSURL *, NSURL *))success downloadPause:(void (^)(id))downloadPause fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    _isNormalDownload = YES;
    return [self resumeDownloadData:url identifier:identifier isBackgroundDownload:isBackgroundDownload resumeData:nil progress:progress success:success downloadPause:downloadPause fail:fail];
}

-(NSURLSessionDownloadTask*)resumeDownloadData:(NSString *)url identifier:(NSString *)identifier isBackgroundDownload:(BOOL)isBackgroundDownload resumeData:(NSData *)resumeData progress:(void (^)(double))progress success:(void (^)(id, NSURL *, NSURL *))success downloadPause:(void (^)(id))downloadPause fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    _downloadIdentifier = identifier?:getRandomNameFor(nil);
    _isBackgroundDownload = isBackgroundDownload;
    self.downloadSuccuss = success;
    self.downloadFail = fail;
    self.downloadPause = downloadPause;
    self.downloadProgress = progress;
    if (!url) {
        _isNormalDownload = NO;
    }
    if (isBackgroundDownload) {
        return [self startBackgroundDownload:url resumeData:resumeData];
    }else{
        return [self startNormalDownload:url resumeData:resumeData];
    }
}

-(void)pauseDownloadTask:(void (^)(NSData *))resumeDataBlock{
    if (_donloadTask) {
        [_donloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
            resumeDataBlock(resumeData);
        }];
    }
}

-(void)cancelDownloadTask{
    if (_donloadTask) {
        [_donloadTask cancel];
    }
}

-(void)moveTempFileForResumeDownload:(NSData*)resumeData{
    NSDictionary *partialDict =  [NSPropertyListSerialization propertyListWithData:resumeData options:NSPropertyListImmutable format:NULL error:nil];
    NSString *tempFileName = [partialDict valueForKey:@"NSURLSessionResumeInfoTempFileName"];
    NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:tempFileName];
    NSString *destFilePath = [[createTempFileurlForCSTDownload() path] stringByAppendingPathComponent:[tempFileName stringByAppendingString:cst_download_resume_tempfile_suffix]];
    [[NSFileManager defaultManager] removeItemAtPath:destFilePath error:nil];
    [[NSFileManager defaultManager] moveItemAtPath:tempFilePath toPath:destFilePath error:nil];
}
-(void)moveFileToTempForResumeDownload:(NSData*)resumeData{
    NSDictionary *partialDict =  [NSPropertyListSerialization propertyListWithData:resumeData options:NSPropertyListImmutable format:NULL error:nil];
    NSString *tempFileName = [partialDict valueForKey:@"NSURLSessionResumeInfoTempFileName"];
    NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:tempFileName];
    NSString *destFilePath = [[createTempFileurlForCSTDownload() path] stringByAppendingPathComponent:[tempFileName stringByAppendingString:cst_download_resume_tempfile_suffix]];
    [[NSFileManager defaultManager] removeItemAtPath:tempFilePath error:nil];
    [[NSFileManager defaultManager] moveItemAtPath:destFilePath toPath:tempFilePath error:nil];
    _downloadResumeData = nil;
}

- (NSData *)correctResumeData:(NSData *)resumeData {
    NSData *newData = nil;
    NSString *kResumeCurrentRequest = @"NSURLSessionResumeCurrentRequest";
    NSString *kResumeOriginalRequest = @"NSURLSessionResumeOriginalRequest";
    NSMutableDictionary* resumeDictionary = [NSPropertyListSerialization propertyListWithData:resumeData options:NSPropertyListMutableContainers format:NULL error:nil];
    resumeDictionary[kResumeCurrentRequest] = [self correctRequestData:resumeDictionary[kResumeCurrentRequest]];
    resumeDictionary[kResumeOriginalRequest] = [self correctRequestData:resumeDictionary[kResumeOriginalRequest]];
    newData = [NSPropertyListSerialization dataWithPropertyList:resumeDictionary format:NSPropertyListBinaryFormat_v1_0 options:NSPropertyListMutableContainers error:nil];
    
    return newData;
}

- (NSData *)correctRequestData:(NSData *)data {
    NSData *resultData = nil;
    NSData *arData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arData != nil) {
        return data;
    }
    
    NSMutableDictionary *archiveDict = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:nil error:nil];
    
    int k = 0;
    NSMutableDictionary *oneDict = [NSMutableDictionary dictionaryWithDictionary:archiveDict[@"$objects"][1]];
    while (oneDict[[NSString stringWithFormat:@"$%d", k]] != nil) {
        k += 1;
    }
    
    int i = 0;
    while (oneDict[[NSString stringWithFormat:@"__nsurlrequest_proto_prop_obj_%d", i]] != nil) {
        NSString *obj = oneDict[[NSString stringWithFormat:@"__nsurlrequest_proto_prop_obj_%d", i]];
        if (obj != nil) {
            [oneDict setObject:obj forKey:[NSString stringWithFormat:@"$%d", i + k]];
            [oneDict removeObjectForKey:obj];
            archiveDict[@"$objects"][1] = oneDict;
        }
        i += 1;
    }
    
    if (oneDict[@"__nsurlrequest_proto_props"] != nil) {
        NSString *obj = oneDict[@"__nsurlrequest_proto_props"];
        [oneDict setObject:obj forKey:[NSString stringWithFormat:@"$%d", i + k]];
        [oneDict removeObjectForKey:@"__nsurlrequest_proto_props"];
        archiveDict[@"$objects"][1] = oneDict;
    }
    
    NSMutableDictionary *twoDict = [NSMutableDictionary dictionaryWithDictionary:archiveDict[@"$top"]];
    if (twoDict[@"NSKeyedArchiveRootObjectKey"] != nil) {
        [twoDict setObject:twoDict[@"NSKeyedArchiveRootObjectKey"] forKey:[NSString stringWithFormat:@"%@", NSKeyedArchiveRootObjectKey]];
        [twoDict removeObjectForKey:@"NSKeyedArchiveRootObjectKey"];
        archiveDict[@"$top"] = twoDict;
    }
    
    resultData = [NSPropertyListSerialization dataWithPropertyList:archiveDict format:NSPropertyListBinaryFormat_v1_0 options:NSPropertyListMutableContainers error:nil];
    
    return resultData;
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    double currentProgress = totalBytesWritten / (double)totalBytesExpectedToWrite;
    _downloadProgress(currentProgress);
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    double currentProgress = fileOffset / (double)expectedTotalBytes;
    _downloadProgress(currentProgress);
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSURL *destinationPath = [createTempFileurlForCSTDownload() URLByAppendingPathComponent:[location lastPathComponent]];
    
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtURL:destinationPath error:NULL];
    
    BOOL success = [[NSFileManager defaultManager] copyItemAtURL:location toURL:destinationPath error:&error];
    
    _downloadProgress(1.0);
    _downloadStatus = CST_DOWNLOAD_STATUS_FINISH;
    
    if (success) {
        _downloadSuccuss(nil,location,destinationPath);
    } else {
        _downloadSuccuss(error,location,nil);
    }
    _removeSelfFromArray(self);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error) {
        NSData *partialData = [error.userInfo valueForKey:@"NSURLSessionDownloadTaskResumeData"];
        _downloadResumeData = partialData;
        if (partialData) {
            _downloadPause(partialData);
            _downloadStatus = CST_DOWNLOAD_STATUS_PAUSE;
            [self moveTempFileForResumeDownload:partialData];
        }else{
            _downloadStatus = CST_DOWNLOAD_STATUS_FAIL;
            _downloadFail(error,NetWork_IsReachable);
            _downloadProgress(0.0);
            _removeSelfFromArray(self);
        }
    }else{
        _downloadStatus = CST_DOWNLOAD_STATUS_FINISH;
        _downloadProgress(1.0);
        _removeSelfFromArray(self);
    }
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    if (_backgroundSessionCompletionHandler) {
        _backgroundSessionCompletionHandler();
    }
}

@end

@implementation CSTDownloadCacheInfo

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _downloadIdentifier = [aDecoder decodeObjectForKey:@"downloadIdentifier"];
        _downloadResumeData = [aDecoder decodeObjectForKey:@"downloadResumeData"];
        _downloadStatus = [[aDecoder decodeObjectForKey:@"downloadStatus"] integerValue];
        _downloadIsBackground = [[aDecoder decodeObjectForKey:@"downloadIsBackground"] boolValue];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_downloadIdentifier forKey:@"downloadIdentifier"];
    [aCoder encodeObject:_downloadResumeData forKey:@"downloadResumeData"];
    [aCoder encodeObject:@(_downloadStatus) forKey:@"downloadStatus"];
    [aCoder encodeObject:@(_downloadIsBackground) forKey:@"downloadIsBackground"];
}

@end


static BOOL isUploadEventData = false;

@interface CSTURLHttpRequest ()

@property (nonatomic, strong) AFHTTPSessionManager *httpManager;
@property (nonatomic, strong) NSMutableArray *cstDownloadRequests;
@property (nonatomic, strong) NSMutableArray *cstCacheRequestinfos;

@end

@implementation CSTURLHttpRequest

+(instancetype)shareManager{
    static CSTURLHttpRequest* public = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!public) {
          
            public = [[CSTURLHttpRequest alloc] init];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            manager.requestSerializer.timeoutInterval = 15;
            public.httpManager = manager;
        }
    });
    return public;
}

+(void)setHttpRequestAndResponseSerializer:(CSTSerializerType)SerializerType{
    switch (SerializerType) {
        case CST_SERIALIZER_REQUEST_RESPONSE_HTTP:
        {
            [CSTURLHttpRequest shareManager].httpManager.requestSerializer = [AFHTTPRequestSerializer serializer];
            [CSTURLHttpRequest shareManager].httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        }
            break;
        case CST_SERIALIZER_REQUEST_RESPONSE_JSON:
        {
            [CSTURLHttpRequest shareManager].httpManager.requestSerializer = [AFJSONRequestSerializer serializer];
            [CSTURLHttpRequest shareManager].httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
        }
            break;
        case CST_SERIALIZER_REQUEST_HTTP_RESPONSE_JSON:
        {
            [CSTURLHttpRequest shareManager].httpManager.requestSerializer = [AFHTTPRequestSerializer serializer];
            [CSTURLHttpRequest shareManager].httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
        }
            break;
        case CST_SERIALIZER_REQUEST_JSON_RESPONSE_HTTP:
        {
            [CSTURLHttpRequest shareManager].httpManager.requestSerializer = [AFJSONRequestSerializer serializer];
            [CSTURLHttpRequest shareManager].httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 私有方法

-(NSArray*)validNormalDownloadRequests{
    NSMutableArray *requests = [NSMutableArray arrayWithCapacity:0];
    for (CSTURLDownloadRequest *request in self.cstDownloadRequests) {
        if (!request.isBackgroundDownload && request.downloadStatus != CST_DOWNLOAD_STATUS_FAIL && request.downloadStatus != CST_DOWNLOAD_STATUS_FINISH) {
            [requests addObject:request];
        }
    }
    return requests;
}
-(NSArray*)validDownloadRequests{
    NSMutableArray *requests = [NSMutableArray arrayWithCapacity:0];
    for (CSTURLDownloadRequest *request in self.cstDownloadRequests) {
        if (request.downloadStatus != CST_DOWNLOAD_STATUS_FAIL && request.downloadStatus != CST_DOWNLOAD_STATUS_FINISH) {
            [requests addObject:request];
        }
    }
    return requests;
}

-(void)archiverRequestsData:(BOOL)cacheAllRequests{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    NSMutableArray *requestinfos = [NSMutableArray arrayWithCapacity:0];
    NSArray *requests = nil;
    if (cacheAllRequests) {
        requests = [NSArray arrayWithArray:[self validDownloadRequests]];
    }else{
        requests = [NSArray arrayWithArray:[self validNormalDownloadRequests]];
    }
    for (CSTURLDownloadRequest *request in requests) {
        if (!request.isBackgroundDownload) {
            [request pauseDownloadTask:^(NSData *resumeData) {
                CSTDownloadCacheInfo *downloadInfo = [[CSTDownloadCacheInfo alloc] init];
                downloadInfo.downloadIdentifier = request.downloadIdentifier;
                downloadInfo.downloadIsBackground = request.isBackgroundDownload;
                downloadInfo.downloadResumeData = resumeData?:request.downloadResumeData;
                downloadInfo.downloadStatus = CST_DOWNLOAD_STATUS_PAUSE;
                [requestinfos addObject:downloadInfo];downloadInfo = nil;
                if (requestinfos.count == requests.count) {
                    dispatch_semaphore_signal(semaphore);
                }
            }];
        }else{
            CSTDownloadCacheInfo *downloadInfo = [[CSTDownloadCacheInfo alloc] init];
            downloadInfo.downloadIdentifier = request.downloadIdentifier;
            downloadInfo.downloadIsBackground = request.isBackgroundDownload;
            downloadInfo.downloadResumeData = (request.downloadStatus == CST_DOWNLOAD_STATUS_PAUSE)?request.downloadResumeData:nil;
            downloadInfo.downloadStatus = request.downloadStatus;
            [requestinfos addObject:downloadInfo];downloadInfo = nil;
            if (requestinfos.count == requests.count) {
                dispatch_semaphore_signal(semaphore);
            }
        }
    }
    if (requests.count > 0) {
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }
    [NSKeyedArchiver archiveRootObject:requestinfos toFile:[[createTempFileurlForCSTDownload() path] stringByAppendingPathComponent:cst_download_cache_file_download_requests]];
}
-(NSArray*)unarchiverRequestsData{
    NSMutableArray *requests = [NSMutableArray arrayWithCapacity:0];
    for (CSTDownloadCacheInfo *info in self.cstCacheRequestinfos) {
        CSTURLDownloadRequest *request = [[CSTURLDownloadRequest alloc] initWithIdentifier:info.downloadIdentifier isBackgroundDownload:info.downloadIsBackground resumeData:info.downloadResumeData];
        [requests addObject:request];
    }
    return requests;
}
-(void)archiverAllRequestsData{
    [self archiverRequestsData:YES];
}
-(void)archiverNormalRequestsData{
    [self archiverRequestsData:YES];
}

-(NSMutableArray*)cstDownloadRequests{
    if (!_cstDownloadRequests) {
        _cstDownloadRequests = [[NSMutableArray alloc] initWithArray:[self unarchiverRequestsData]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(archiverNormalRequestsData) name:UIApplicationWillResignActiveNotification object:nil ];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(archiverAllRequestsData) name:UIApplicationWillTerminateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(archiverAllRequestsData) name:cst_download_crash_notifcation object:nil];
        NSSetUncaughtExceptionHandler(&CST_ExceptionCaptureHander);
    }
    return _cstDownloadRequests;
}

-(NSMutableArray *)cstCacheRequestinfos{
    if (!_cstCacheRequestinfos) {
        _cstCacheRequestinfos = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:[[createTempFileurlForCSTDownload() path] stringByAppendingPathComponent:cst_download_cache_file_download_requests]]];
    }
    return _cstCacheRequestinfos;
}

+(CSTURLDownloadRequest*)getDownloadRequestWithIdentifier:(NSString*)identifier{
    if (identifier) {
        NSMutableArray *requests = [[CSTURLHttpRequest shareManager] cstDownloadRequests];
        for (CSTURLDownloadRequest *request in requests) {
            if ([request.downloadIdentifier isEqualToString:identifier]) {
                return request;
            }
        }
    }
    return nil;
}

+(NSString *)getMIMETypeWithCAPIAtFilePath:(NSString *)path
{
    if (![[[NSFileManager alloc] init] fileExistsAtPath:path]) {
        return nil;
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!MIMEType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)(MIMEType)
    ;
}
+(NSString*)getExistMimeTypeFor:(NSString*)type{
    if (!type || [type length] <= 0) {
        type = @"application/octet-stream";
    }
    return type;
}

#if __has_include("NSData+CSTGzip.h")

#pragma mark - 上传事件统计方法

+(void)postEvent{
    if (!isUploadEventData) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:Event_Path]) {
            NSMutableArray* array = [NSMutableArray arrayWithContentsOfFile:Event_Path];
            //转json
            NSString *jsonStr = [array JSONString];
            //转data
            NSData *data = [jsonStr dataUsingEncoding: NSUTF8StringEncoding];
            //压缩gzip data
            data = [data gzippedData];
            
            if (array.count > 0 && NetWork_IsReachable) {
                //上传之前先删除文件，防止上传过程中有数据再次写入而没有被上传
                [[NSFileManager defaultManager] copyItemAtPath:Event_Path toPath:Event_Path_Temp error:nil];
                [[NSFileManager defaultManager] removeItemAtPath:Event_Path error:nil];
                
                NSUserDefaults* def = [NSUserDefaults standardUserDefaults];
                NSDictionary* dict = @{};
                isUploadEventData = true;
                AFHTTPSessionManager * managerHttp = [AFHTTPSessionManager manager];
                managerHttp.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",nil];
                NSString *userAgent = [[managerHttp.requestSerializer HTTPRequestHeaders] valueForKey:@"User-Agent"];
                userAgent = [userAgent stringByAppendingFormat:@" package=cn.shangji.ctvonline;channel=%@;version=%@;imei=%@;",[def valueForKey:@""],[def valueForKey:@""],[def valueForKey:@""]];
                [managerHttp.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
                [managerHttp POST:[@"" stringByAppendingString:@"v2/userAction"] parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                    [formData appendPartWithFileData:data name:@"gzipByte" fileName:@"gzipByte" mimeType:@"text/plain"];
                } progress:^(NSProgress * _Nonnull uploadProgress) {
                    
                } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    if ([[responseObject valueForKey:@"respondcode"] intValue] == 1) {
                        [[NSFileManager defaultManager] removeItemAtPath:Event_Path_Temp error:nil];
                    }else{
                        [self disponseUploadFailEvent];
                    }
                    isUploadEventData = false;
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    [self disponseUploadFailEvent];
                    isUploadEventData = false;
                }];
            }
        }
    }
}

+(void)disponseUploadFailEvent{
    NSArray* array = [NSArray array];
    if ([[NSFileManager defaultManager] fileExistsAtPath:Event_Path]) {
        array = [NSArray arrayWithArray:[NSArray arrayWithContentsOfFile:Event_Path]];
    }
    if (array) {
        array = [array arrayByAddingObjectsFromArray:[NSArray arrayWithContentsOfFile:Event_Path_Temp]];
        [array writeToFile:Event_Path atomically:YES];
    }
    [[NSFileManager defaultManager] removeItemAtPath:Event_Path_Temp error:nil];
}

#endif

#pragma mark - setUserAgent;

+(void)setUserAgent:(NSString *)userAgent idfa:(NSString *)adfa{
  [[CSTURLHttpRequest shareManager].httpManager.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
  [[CSTURLHttpRequest shareManager].httpManager.requestSerializer setValue:userAgent forHTTPHeaderField:@"X-User-Agent"];
  [[CSTURLHttpRequest shareManager].httpManager.requestSerializer setValue:adfa forHTTPHeaderField:@"X-Device-Id"];
}

#pragma mark - 网络监测

+(void)startNetMonitoring:(void(^)(AFNetworkReachabilityStatus status))netStatus{
    AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    [reachabilityManager startMonitoring];
    [reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        netStatus(status);
    }];
}

+(void)startSSLRequest{
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"appdu" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    securityPolicy.allowInvalidCertificates = NO;
    securityPolicy.validatesDomainName = YES;
    securityPolicy.pinnedCertificates = [NSSet setWithObjects:certData, nil];
    [[[CSTURLHttpRequest shareManager] httpManager] setSecurityPolicy:securityPolicy];
}

+(void)cancelSSLRequest{
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [[[CSTURLHttpRequest shareManager] httpManager] setSecurityPolicy:securityPolicy];
}

#pragma mark - get post 方法

+(NSURLSessionDataTask*)get:(NSString*)url
                      parms:(NSDictionary*)dict
                    success:(void (^)(id responseObject))success
                       fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        AFHTTPSessionManager *manager = [[CSTURLHttpRequest shareManager] httpManager];
        NSURLSessionDataTask *dataTask = [manager GET:url parameters:dict progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            fail(error,reachable);
        }];
        return dataTask;
    }else{
        fail(NetWork_NetNoneText,reachable);
        return nil;
    }
}

+(NSURLSessionDataTask*)post:(NSString*)url
                       parms:(NSDictionary*)dict
                     success:(void (^)(id responseObject))success
                        fail:(void(^)(id error,AFNetworkReachabilityStatus reachable))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        AFHTTPSessionManager *manager = [[CSTURLHttpRequest shareManager] httpManager];
        NSURLSessionDataTask *dataTask = [manager POST:url parameters:dict progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            fail(error,reachable);
        }];
        return dataTask;
    }else{
        fail(NetWork_NetNoneText,reachable);
        return nil;
    }
}

#pragma mark - 上传文件，多种方式

+(NSURLSessionUploadTask*)uploadUseFile:(NSString *)url filePath:(NSString *)filePath params:(NSDictionary *)dict progress:(void (^)(NSProgress *))progress success:(void (^)(id))success fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    return [self uploadUseFiles:url filePath:@[filePath] params:dict progress:progress success:success fail:fail];
}
+(NSURLSessionUploadTask*)uploadUseFiles:(NSString *)url filePath:(NSArray *)filesPath params:(NSDictionary *)dict progress:(void (^)(NSProgress *))progress success:(void (^)(id))success fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSURLSessionUploadTask *uploadTask = (NSURLSessionUploadTask*)[manager POST:url parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            for (NSString *filePath in filesPath) {
                [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath] name:@"file" fileName:getRandomNameFor([filePath lastPathComponent]) mimeType:[self getMIMETypeWithCAPIAtFilePath:filePath] error:nil];
            }
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            progress(uploadProgress);
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            fail(error,reachable);
        }];
        return uploadTask;
    }else{
        fail(NetWork_NetNoneText,reachable);
        return nil;
    }
}
+(NSURLSessionUploadTask*)uploadUseData:(NSString *)url dataInfo:(NSDictionary *)info params:(NSDictionary *)dict progress:(void (^)(NSProgress *))progress success:(void (^)(id))success fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    return [self uploadUseDatas:url dataInfos:@[info] params:dict progress:progress success:success fail:fail];
}
+(NSURLSessionUploadTask*)uploadUseDatas:(NSString *)url dataInfos:(NSArray *)infos params:(NSDictionary *)dict progress:(void (^)(NSProgress *))progress success:(void (^)(id))success fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSURLSessionUploadTask *uploadTask = (NSURLSessionUploadTask*)[manager POST:url parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            for (NSDictionary *info in infos) {
                [formData appendPartWithFileData:info[@"data"] name:@"file" fileName:getRandomNameFor(info[@"filename"]) mimeType:[self getExistMimeTypeFor:info[@"mimeType"]]];
            }
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            progress(uploadProgress);
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            fail(error,reachable);
        }];
        return uploadTask;
    }else{
        fail(NetWork_NetNoneText,reachable);
        return nil;
    }
}

#pragma mark - 下载文件，支持断点续传

+(void)downloadData:(NSString *)url identifier:(NSString *)identifier isBackgroundDownload:(BOOL)isBackgroundDownload progress:(void (^)(double))progress success:(void (^)(id, NSURL *, NSURL *))success downloadPause:(void (^)(id))downloadPause fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        if ([self getDownloadRequestWithIdentifier:identifier]) {
            fail([NSError errorWithDomain:@"已有相同任务" code:10000502 userInfo:nil],reachable);
        }else{
            CSTURLDownloadRequest *downloadRequest = [[CSTURLDownloadRequest alloc] init];
            [downloadRequest setRemoveSelfFromArray:^(id refSelf) {
                [[[CSTURLHttpRequest shareManager] cstDownloadRequests] removeObject:refSelf];
            }];
            [downloadRequest downloadData:url identifier:identifier isBackgroundDownload:isBackgroundDownload progress:progress success:success downloadPause:downloadPause fail:fail];
            [[[CSTURLHttpRequest shareManager] cstDownloadRequests] addObject:downloadRequest];
        }
    }else{
        fail(NetWork_NetNoneText,reachable);
    }
}
+(void)resumeDownloadData:(NSData *)resumeData isBackgroundDownload:(BOOL)isBackgroundDownload progress:(void (^)(double))progress success:(void (^)(id, NSURL *, NSURL *))success downloadPause:(void (^)(id))downloadPause fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        CSTURLDownloadRequest *downloadRequest = [[CSTURLDownloadRequest alloc] init];
        [downloadRequest setRemoveSelfFromArray:^(id refSelf) {
            [[[CSTURLHttpRequest shareManager] cstDownloadRequests] removeObject:refSelf];
        }];
        [downloadRequest resumeDownloadData:nil identifier:nil isBackgroundDownload:isBackgroundDownload resumeData:resumeData progress:progress success:success downloadPause:downloadPause fail:fail];
        [[[CSTURLHttpRequest shareManager] cstDownloadRequests] addObject:downloadRequest];
    }else{
        fail(NetWork_NetNoneText,reachable);
    }
}
+(void)resumeDownloadIdentifier:(NSString *)identifier isBackgroundDownload:(BOOL)isBackgroundDownload progress:(void (^)(double))progress success:(void (^)(id, NSURL *, NSURL *))success downloadPause:(void (^)(id))downloadPause fail:(void (^)(id, AFNetworkReachabilityStatus))fail{
    AFNetworkReachabilityStatus reachable = NetWork_IsReachable;
    if (reachable) {
        CSTURLDownloadRequest *downloadRequest = [self getDownloadRequestWithIdentifier:identifier];
        NSURLSessionDownloadTask *downloadTask = nil;
        if (!downloadRequest) {
            fail([NSError errorWithDomain:@"继续下载不可用，需要重新下载" code:10000503 userInfo:nil],reachable);
        }else{
            if ((downloadRequest.downloadStatus == CST_DOWNLOAD_STATUS_PAUSE) || (downloadRequest.downloadStatus == CST_DOWNLOAD_STATUS_LOADING && downloadRequest.isBackgroundDownload && !downloadRequest.downloadSession)) {
                [downloadRequest setRemoveSelfFromArray:^(id refSelf) {
                    [[[CSTURLHttpRequest shareManager] cstDownloadRequests] removeObject:refSelf];
                }];
                downloadTask = [downloadRequest resumeDownloadData:nil identifier:identifier isBackgroundDownload:isBackgroundDownload resumeData:downloadRequest.downloadResumeData progress:progress success:success downloadPause:downloadPause fail:fail];
            }else if (downloadRequest.downloadStatus == CST_DOWNLOAD_STATUS_LOADING) {
                fail([NSError errorWithDomain:@"已有相同任务，请不要重复下载" code:10000504 userInfo:nil],reachable);
            }else{
                [[[CSTURLHttpRequest shareManager] cstDownloadRequests] removeObject:downloadRequest];
                fail([NSError errorWithDomain:@"继续下载不可用，需要重新下载" code:10000503 userInfo:nil],reachable);
            }
        }
    }else{
        fail(NetWork_NetNoneText,reachable);
    }
}

+(void)setBackgroundSessionCompletionHandlerWithIdentifier:(NSString*)identifier completionHandler:(void (^)())completionHandler{
    if ([identifier hasPrefix:cst_download_type_background]) {
        identifier = [identifier substringFromIndex:cst_download_type_background.length];
    }
    CSTURLDownloadRequest *request = [self getDownloadRequestWithIdentifier:identifier];
    if (request) {
        request.backgroundSessionCompletionHandler = completionHandler;
        completionHandler = nil;
    }
}

#pragma mark - 暂停、关闭任务等方法

+(CSTDownloadCacheInfo*)downloadInfoWithIdentifier:(NSString*)identifier{
    if ([[CSTURLHttpRequest shareManager] cstDownloadRequests]) {
        for (CSTDownloadCacheInfo *info in [[CSTURLHttpRequest shareManager] cstCacheRequestinfos]) {
            if ([info.downloadIdentifier isEqualToString:identifier]) {
                CSTDownloadCacheInfo *tempInfo = [[CSTDownloadCacheInfo alloc] init];
                tempInfo.downloadIdentifier = info.downloadIdentifier;
                tempInfo.downloadResumeData = info.downloadResumeData;
                tempInfo.downloadIsBackground = info.downloadIsBackground;
                tempInfo.downloadStatus = info.downloadStatus;
                [[[CSTURLHttpRequest shareManager] cstCacheRequestinfos] removeObject:info];
                return tempInfo;
            }
        }
    }
    return nil;
}

+(void)pauseDownload:(NSString *)identifier resumeDataBlock:(void (^)(NSData *))resumeDataBlock{
    CSTURLDownloadRequest *request = [self getDownloadRequestWithIdentifier:identifier];
    if (request) {
        [request pauseDownloadTask:^(NSData *resumeData) {
            resumeDataBlock(resumeData);
        }];
    }else{
        resumeDataBlock(nil);
    }
}
+(void)cancelDownload:(NSString*)identifier{
    CSTURLDownloadRequest *request = [self getDownloadRequestWithIdentifier:identifier];
    if (request) {
        [request cancelDownloadTask];
    }
}
+(void)cancelAllDownloads{
    for (CSTURLDownloadRequest *request in [[CSTURLHttpRequest shareManager] cstDownloadRequests]) {
        [request cancelDownloadTask];
    }
}
+(void)cancleRequest:(NSURLSessionDataTask*)dataTask{
    if (dataTask) {
        [dataTask cancel];
    }
}
+(void)cancleAllRequest{
    AFHTTPSessionManager *manager = [[CSTURLHttpRequest shareManager] httpManager];
    if (manager) {
        [manager.session invalidateAndCancel];
    }
}

@end
