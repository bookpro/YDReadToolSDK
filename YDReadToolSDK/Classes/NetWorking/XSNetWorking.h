//
//  XSNetWorking.h
//  novels_ebook
//
//  Created by Chen Qian on 2017/5/9.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSNetWorking : NSObject

@property (nonatomic,copy) NSArray *chapterList;
@property (nonatomic,copy) NSString *chapterOriginID;
@property (nonatomic,copy) NSMutableArray *novelDownloadRequests;
+(instancetype)shareManager;
+(void)loadChapterList:(NSString*)bookId originId:(NSString*)originId callback:(void (^)(id chaptersList))callback;
+(void)loadChapterContent:(NSString*)urlSuff callback:(void (^)(NSString *content))callback;
+(void)loadChapterOrigin:(NSString*)bookId callback:(void (^)(id originList))callback;
+(void)postPayInfo:(NSDictionary*)params;

+(void)loadRankingData:(void(^)(NSDictionary*))callback;
+(void)loadCategoryData:(void (^)(NSDictionary *))callback;
+(void)loadSearchKeysData:(void (^)(NSDictionary *))callback;
+(void)loadSearchListData:(NSString*)searchKey callback:(void (^)(NSDictionary *))callback;
+(void)loadSubRankingListData:(NSString*)rankingId callback:(void (^)(NSDictionary *))callback;
+(void)loadSubCategoryListData:(NSDictionary*)info page:(NSInteger)pageIndex callback:(void (^)(NSDictionary *))callback;
+(void)loadDetailCoverData:(NSString*)rankingId callback:(void (^)(NSDictionary *))callback;
+(void)loadCoverRecommentData:(NSString*)rankingId callback:(void (^)(NSDictionary *))callback;
+(void)loadAuthorBooksData:(NSString*)author callback:(void (^)(NSDictionary *))callback;
+(void)loadBooksUpdateData:(NSArray*)ids callback:(void (^)(NSArray *))callback;

+(void)loadBookListData:(NSDictionary*)params page:(NSInteger)pageIndex callback:(void (^)(NSDictionary *))callback;
+(void)loadBookListSubData:(NSString*)booklistId callback:(void (^)(NSDictionary *))callback;

@end
