//
//  XSNetWorking.m
//  novels_ebook
//
//  Created by Chen Qian on 2017/5/9.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "XSNetWorking.h"
#import "CSTURLHttpRequest.h"
#import "NSString+URLEncoding.h"

static const NSInteger request_count_per_page = 50;

@implementation XSNetWorking

+(instancetype)shareManager{
    static XSNetWorking* public = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!public) {
            public = [[XSNetWorking alloc] init];
        }
    });
    return public;
}

+(void)loadChapterList:(NSString*)bookId originId:(NSString*)originId callback:(void (^)(id chaptersList))callback{
    if (originId && originId.length > 0) {
        [self loadRealChapterList:originId originName:nil callback:callback];
    }else{
        [self loadChapterOrigin:bookId callback:^(id originList) {
            if ([originList isKindOfClass:[NSArray class]]) {
                NSArray *originArray = [NSArray arrayWithArray:originList];
                BOOL findOriginData = false;
                for (NSDictionary *dict in originArray) {
                    if ([[dict valueForKey:@"source"] containsString:@"zhuishu"]) {
                        continue;
                    }
                    NSString * tprealOriginId = [dict valueForKey:@"_id"];
                    if (tprealOriginId && tprealOriginId.length > 0) {
                        findOriginData = true;
                        [self loadRealChapterList:tprealOriginId originName:nil callback:callback];
                        break;
                    }
                }
                if (findOriginData == false) {
                    if (originArray.count > 0) {
                        NSDictionary *dict = [originList lastObject];
                        NSString * tprealOriginId = [dict valueForKey:@"_id"];
                        if (tprealOriginId && tprealOriginId.length > 0) {
                            findOriginData = true;
                            [self loadRealChapterList:tprealOriginId originName:@"zhuishu" callback:callback];
                        }
                    }
                    if (findOriginData == false && callback) {
                        callback(false);
                    }
                }
            }else{
                NSLog(@"nimade");
                if (callback) {
                    callback(false);
                }
            }
        }];
    }
}

+(void)loadChapterOrigin:(NSString*)bookId callback:(void (^)(id originList))callback{
    [CSTURLHttpRequest get:[@"http://api.zhuishushenqi.com/atoc?view=summary&book=" stringByAppendingString:bookId] parms:nil success:^(id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            callback(responseObject);
        }else{
            [self loadChapterOrigin2:bookId callback:callback];
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        [self loadChapterOrigin2:bookId callback:callback];
    }];
}

+(void)loadChapterOrigin2:(NSString*)bookId callback:(void (^)(id originList))callback{
    [CSTURLHttpRequest get:[@"http://api.zhuishushenqi.com/toc?view=summary&book=" stringByAppendingString:bookId] parms:nil success:^(id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            callback(responseObject);
        }else{
            [self loadChapterOrigin3:bookId callback:callback];
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        [self loadChapterOrigin3:bookId callback:callback];
    }];
}

+(void)loadChapterOrigin3:(NSString*)bookId callback:(void (^)(id originList))callback{
    [CSTURLHttpRequest get:[@"http://api.zhuishushenqi.com/btoc?view=summary&book=" stringByAppendingString:bookId] parms:nil success:^(id responseObject) {
        callback(responseObject);
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        callback(error);
    }];
}

+(void)loadRealChapterList:(NSString*)originId originName:(NSString*)originName callback:(void (^)(id chaptersList))callback{
    [CSTURLHttpRequest get:[NSString stringWithFormat:@"http://api.zhuishushenqi.com/atoc/%@?view=chapters",originId] parms:nil success:^(id responseObject) {
        if (responseObject && callback) {
            NSArray *chaptersArray = [NSArray arrayWithArray:[responseObject valueForKey:@"chapters"]];
            NSMutableArray *realChapterArray = [NSMutableArray arrayWithCapacity:0];
            if ([originName isEqualToString:@"zhuishu"]) {
                for (int i = 0; i < chaptersArray.count; i++) {
                    NSDictionary *dict = chaptersArray[i];
                    if ([dict[@"isVip"] boolValue] == false) {
                        [realChapterArray addObject:dict];
                    }
                }
            }else{
                [realChapterArray addObjectsFromArray:chaptersArray];
            }
            [XSNetWorking shareManager].chapterOriginID = originId;
            [XSNetWorking shareManager].chapterList = realChapterArray;
            callback(realChapterArray);
        }else{
            [self loadRealChapterList2:originId originName:originName callback:callback];
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        [self loadRealChapterList2:originId originName:originName callback:callback];
    }];
}

+(void)loadRealChapterList2:(NSString*)originId originName:(NSString*)originName callback:(void (^)(id chaptersList))callback{
    [CSTURLHttpRequest get:[NSString stringWithFormat:@"http://api.zhuishushenqi.com/toc/%@?view=chapters",originId] parms:nil success:^(id responseObject) {
        if (responseObject && callback) {
            NSArray *chaptersArray = [NSArray arrayWithArray:[responseObject valueForKey:@"chapters"]];
            NSMutableArray *realChapterArray = [NSMutableArray arrayWithCapacity:0];
            if ([originName isEqualToString:@"zhuishu"]) {
                for (int i = 0; i < chaptersArray.count; i++) {
                    NSDictionary *dict = chaptersArray[i];
                    if ([dict[@"isVip"] boolValue] == false) {
                        [realChapterArray addObject:dict];
                    }
                }
            }else{
                [realChapterArray addObjectsFromArray:chaptersArray];
            }
            [XSNetWorking shareManager].chapterOriginID = originId;
            [XSNetWorking shareManager].chapterList = realChapterArray;
            callback(realChapterArray);
        }else{
            [self loadRealChapterList3:originId originName:originName callback:callback];
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        [self loadRealChapterList3:originId originName:originName callback:callback];
    }];
}

+(void)loadRealChapterList3:(NSString*)originId originName:(NSString*)originName callback:(void (^)(id chaptersList))callback{
    [CSTURLHttpRequest get:[NSString stringWithFormat:@"http://api.zhuishushenqi.com/btoc/%@?view=chapters",originId] parms:nil success:^(id responseObject) {
        if (responseObject && callback) {
            NSArray *chaptersArray = [NSArray arrayWithArray:[responseObject valueForKey:@"chapters"]];
            NSMutableArray *realChapterArray = [NSMutableArray arrayWithCapacity:0];
            if ([originName isEqualToString:@"zhuishu"]) {
                for (int i = 0; i < chaptersArray.count; i++) {
                    NSDictionary *dict = chaptersArray[i];
                    if ([dict[@"isVip"] boolValue] == false) {
                        [realChapterArray addObject:dict];
                    }
                }
            }else{
                [realChapterArray addObjectsFromArray:chaptersArray];
            }
            [XSNetWorking shareManager].chapterOriginID = originId;
            [XSNetWorking shareManager].chapterList = realChapterArray;
            callback(realChapterArray);
        }else{
            callback(@"没有获取到章节数据");
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        callback(error);
    }];
}

+(void)loadChapterContent:(NSString*)urlSuff callback:(void (^)(NSString *content))callback{
    
    NSString *url = [@"http://chapter2.zhuishushenqi.com/chapter/" stringByAppendingString:[urlSuff URLEncodedStringCut]];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        NSString *bodyStr = [NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"chapter"] valueForKey:@"body"]];
        if ([bodyStr containsString:@"安装最新版"] || [bodyStr containsString:@"追书神器"]) {
            bodyStr = [NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"chapter"] valueForKey:@"cpContent"]];
        }
        if ([bodyStr containsString:@"当前使用的版本过低"] && [bodyStr containsString:@"已停止服务"]) {
            bodyStr = @"\n\n当前章节源无内容，您可点击右上角换源按钮获取更多资源。";
        }
        if ([[responseObject valueForKey:@"ok"] intValue] == 1 && bodyStr.length > 0) {
            callback(bodyStr);
        }else{
            callback(@"error");
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        callback(@"error");
    }];
}

+(void)loadRankingData:(void (^)(NSDictionary *))callback{
    NSString *url = @"http://api.zhuishushenqi.com/ranking/gender";
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }else{
            
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadCategoryData:(void (^)(NSDictionary *))callback{
    NSString *url = @"http://api.zhuishushenqi.com/cats/lv2/statistics";
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }else{
            
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadSubRankingListData:(NSString *)rankingId callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/ranking/%@",rankingId];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }else{
            
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadSubCategoryListData:(NSDictionary *)info page:(NSInteger)pageIndex callback:(void (^)(NSDictionary *))callback{
    NSInteger startIndex = pageIndex*request_count_per_page;
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/by-categories?gender=%@&type=%@&major=%@&start=%ld&limit=50",info[@"gender"],info[@"type"],[info[@"_id"] URLEncodedString],startIndex];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }else{
            
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadDetailCoverData:(NSString *)rankingId callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/%@",rankingId];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        callback(responseObject);
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadCoverRecommentData:(NSString *)rankingId callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/%@/recommend",rankingId];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadAuthorBooksData:(NSString *)author callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/accurate-search?author=%@",[author URLEncodedString]];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadSearchKeysData:(void (^)(NSDictionary *))callback{
    NSString *url = @"http://api.zhuishushenqi.com/book/hot-word";
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadSearchListData:(NSString *)searchKey callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/fuzzy-search?query=%@",[searchKey URLEncodedString]];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        if ([[responseObject valueForKey:@"ok"] boolValue]) {
            callback(responseObject);
        }
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadBooksUpdateData:(NSArray*)ids callback:(void (^)(NSArray *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book?view=updated&id=%@",[ids componentsJoinedByString:@","]];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        callback(responseObject);
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadBookListData:(NSDictionary *)params page:(NSInteger)pageIndex callback:(void (^)(NSDictionary *))callback{
    NSInteger startIndex = pageIndex*20;
    NSString *url = @"http://api.zhuishushenqi.com/book-list";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:params];
    [dict setObject:@(startIndex) forKey:@"start"];
    [dict setObject:@(20) forKey:@"limit"];
    [CSTURLHttpRequest get:url parms:dict success:^(id responseObject) {
        callback(responseObject);
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

+(void)loadBookListSubData:(NSString*)booklistId callback:(void (^)(NSDictionary *))callback{
    NSString *url = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book-list/%@",booklistId];
    [CSTURLHttpRequest get:url parms:nil success:^(id responseObject) {
        callback(responseObject);
    } fail:^(id error, AFNetworkReachabilityStatus reachable) {
        
    }];
}

@end
