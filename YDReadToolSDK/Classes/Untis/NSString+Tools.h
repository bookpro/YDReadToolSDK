//
//  NSString+Tools.h
//  NovelRead
//
//  Created by Chen Qian on 2018/3/19.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Tools)

+(NSString *)getLocalDateFormateUTCDate:(NSString*)utcDate;
+(BOOL)compareDate:(NSString*)updateDate curdate:(NSString*)curdate;

@end
