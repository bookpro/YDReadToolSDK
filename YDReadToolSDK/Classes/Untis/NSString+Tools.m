//
//  NSString+Tools.m
//  NovelRead
//
//  Created by Chen Qian on 2018/3/19.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import "NSString+Tools.h"

@implementation NSString (Tools)

+(NSString *)getLocalDateFormateUTCDate:(NSString*)utcDate
{
    utcDate = [utcDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    utcDate = [utcDate stringByReplacingOccurrencesOfString:@"Z" withString:@""];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSDate *timeDate = [dateFormatter dateFromString:utcDate];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:timeDate];
    NSString *result;
    long temp = 0;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%ld分钟前",temp];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    
    else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%ld天前",temp];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld月前",temp];
    }
    else{
        temp = temp/12;
        result = [NSString stringWithFormat:@"%ld年前",temp];
    }
    
    dateFormatter = nil;
    
    return  result;
}

+(BOOL)compareDate:(NSString *)updateDate curdate:(NSString *)curdate{
    updateDate = [updateDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    updateDate = [updateDate stringByReplacingOccurrencesOfString:@"Z" withString:@""];
    curdate = [curdate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    curdate = [curdate stringByReplacingOccurrencesOfString:@"Z" withString:@""];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    
    NSDate *updateDateDate = [dateFormatter dateFromString:updateDate];
    NSDate *curdateDate = [dateFormatter dateFromString:curdate];
    NSTimeInterval timeInterval = [curdateDate timeIntervalSinceDate:updateDateDate];
    dateFormatter = nil;
    if (timeInterval > 0) {
        return YES;
    }else{
        return  NO;
    }
}

@end
