//
//  NSString+URLEncoding.h
//  AskDoctor
//
//  Created by Chen Qian on 13-7-2.
//  Copyright (c) 2013年 Chen Qian. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 字符串url编码解码
 */
@interface NSString (URLEncoding)


/**
 url编码

 @return return value description
 */
- (NSString *)URLEncodedString;


/**
 url解码

 @return return value description
 */
- (NSString *)URLDecodedString;

/**
 url解码删减版
 
 @return return value description
 */
- (NSString *)URLEncodedStringCut;

@end
