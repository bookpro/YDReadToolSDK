//
//  NSString+URLEncoding.m
//  AskDoctor
//
//  Created by Chen Qian on 13-7-2.
//  Copyright (c) 2013年 Chen Qian. All rights reserved.
//

#import "NSString+URLEncoding.h"

@implementation NSString (URLEncoding)

- (NSString *)URLEncodedString{
    __autoreleasing NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,
                                                                           CFSTR("!*'^_^‘’();:@&=+$,/?%#[]"),
                                                                           kCFStringEncodingUTF8));
    return result;
}

- (NSString*)URLDecodedString{
    __autoreleasing NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                                           (CFStringRef)self,
                                                                                           CFSTR(""),
                                                                                           kCFStringEncodingUTF8));
    return result;
}

- (NSString *)URLEncodedStringCut{
  __autoreleasing NSString* result = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                          (CFStringRef)self,
                                                                                                          NULL,
                                                                                                          CFSTR("!*'();@&+$,/?%#[]"),
                                                                                                          kCFStringEncodingUTF8));
  return result;
}

@end
