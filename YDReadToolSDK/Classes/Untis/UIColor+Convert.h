//
//  UIColor+Convert.h
//  novels_ebook
//
//  Created by Chen Qian on 2017/4/20.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Convert)

+ (UIColor *) colorWithHex: (NSString *)color;

@end
