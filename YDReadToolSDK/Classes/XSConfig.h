//
//  XSConfig.h
//  NovelRead
//
//  Created by Chen Qian on 2018/3/15.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#ifndef XSConfig_h
#define XSConfig_h

#import "UIColor+Convert.h"

static NSString* navigation_background_color = @"F5F5F5";
static NSString* background_black_color = @"212121";
static NSString* navigation_theme_color = @"2765EA";
static NSString* button_theme_background_color = @"2765EA";
static NSString* button_gray_background_color = @"909090";
static NSString* line_background_color = @"F4F4F4";
static NSString* red_text_color = @"B73628";
static NSString* gray_text_color = @"7B7B7B";
static NSString* back_text_color = @"222222";
static NSString* gray_px_line_color = @"D2D2D2";

#endif /* XSConfig_h */
