//
//  XSOriginsController.h
//  novels_ebook
//
//  Created by Chen Qian on 2017/5/12.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XSOriginsController : UITableViewController

@property (nonatomic,copy) NSString *bookId;
-(instancetype)initWithOrigins:(NSArray*)origins callback:(void(^)(NSArray *chapterList))callback;

@end
