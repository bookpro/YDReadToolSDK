//
//  XSOriginsController.m
//  novels_ebook
//
//  Created by Chen Qian on 2017/5/12.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "XSOriginsController.h"
#import "XSNetWorking.h"
#import "XSConfig.h"
#import "MBProgressHUD+DZM.h"

typedef void(^CallbackOriginId)(NSArray *chapterList);

@interface XSOriginsController ()

@property (nonatomic,strong) NSMutableArray *dataOrigins;
@property (nonatomic,copy) CallbackOriginId callbackOriginId;
@property (nonatomic,readonly) NSString *textColor;


@end

@implementation XSOriginsController

-(instancetype)initWithOrigins:(NSArray*)origins callback:(void (^)(NSArray *))callback{
  self = [super init];
  if (self) {
    self.dataOrigins = [NSMutableArray arrayWithArray:origins];
    self.callbackOriginId = callback;
    for (int i = 0; i < _dataOrigins.count; i++) {
      NSDictionary *dict = _dataOrigins[i];
      if ([[dict valueForKey:@"source"] containsString:@"zhuishu"]) {
        [_dataOrigins removeObjectAtIndex:i];
        break;
      }
    }
  }
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择书源";
    
    _textColor = @"bcbcbc";
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
  
    self.view.backgroundColor = [UIColor blackColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorColor = [UIColor colorWithHex:_textColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHex:@"000000"]];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:_textColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:_textColor]}];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataOrigins.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell_Origins"];
    if (!cell) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"UITableViewCell_Origins"];
      cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor colorWithHex:_textColor];
        cell.detailTextLabel.textColor = [UIColor colorWithHex:_textColor];
        [cell.detailTextLabel setFont:[UIFont systemFontOfSize:14]];
    }
    cell.textLabel.text = [_dataOrigins[indexPath.row] valueForKey:@"name"];
    cell.detailTextLabel.text = [_dataOrigins[indexPath.row] valueForKey:@"lastChapter"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  if (_callbackOriginId) {
    [MBProgressHUD showMessage:@"正在加载章节列表" toView:self.view];
    __weak typeof(self) tpself = self;
    [XSNetWorking loadChapterList:_bookId originId:[_dataOrigins[indexPath.row] valueForKey:@"_id"] callback:^(id chaptersList) {
      if ([chaptersList isKindOfClass:[NSArray class]]) {
        tpself.callbackOriginId(chaptersList);
      }
      [MBProgressHUD hideHUDForView:tpself.view animated:YES];
      [tpself.navigationController popViewControllerAnimated:YES];
    }];
  }else{
    [self.navigationController popViewControllerAnimated:YES];
  }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
