//
//  XSVoiceSynthesizer.h
//  NovelRead
//
//  Created by Chen Qian on 2018/3/23.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface XSVoiceSynthesizer : NSObject<AVSpeechSynthesizerDelegate>

@property(nonatomic,assign)float rate;   //语速

@property(nonatomic,assign)float volume; //音量

@property(nonatomic,assign)float pitchMultiplier;  //音调

@property(nonatomic,assign)float postUtteranceDelay;  //停顿时间

@property(nonatomic,assign)NSInteger selectVoiceIndex;  //音调

@property(nonatomic,assign)BOOL  autoPlay;  //自动播放

@property(nonatomic,strong)NSArray *voices;

@property(nonatomic,strong)NSMutableArray *speechStrings;

@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer; //语音合成

+(instancetype)shareManager;

//播放并给出文字

-(void)play:(NSString *)string;

-(void)replay;

-(void)stop;

-(void)pause;

-(void)contine;

@end
