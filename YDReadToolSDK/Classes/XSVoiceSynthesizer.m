//
//  XSVoiceSynthesizer.m
//  NovelRead
//
//  Created by Chen Qian on 2018/3/23.
//  Copyright © 2018年 Chen Qian. All rights reserved.
//

#import "XSVoiceSynthesizer.h"

@implementation XSVoiceSynthesizer

+(instancetype)shareManager{
    static XSVoiceSynthesizer* singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!singleton) {
            singleton = [[XSVoiceSynthesizer alloc] init];
            [singleton singletonInit];
        }
    });
    return singleton;
}

-(void)singletonInit{
    //创建语音合成器
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];
    self.synthesizer.delegate = self;
    //播放的国家的语言
    self.voices = @[[AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"],
                    [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-HK"],
                    [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-TW"]];
    self.rate = MAX(0.5, [[NSUserDefaults standardUserDefaults] floatForKey:@"XSVoiceSynthesizer_voice_rate"]);
    self.volume = 1.0f;
    self.pitchMultiplier = MAX(0.8, [[NSUserDefaults standardUserDefaults] floatForKey:@"XSVoiceSynthesizer_voice_pm"]);
    self.postUtteranceDelay = 0.0f;
    self.selectVoiceIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"XSVoiceSynthesizer_voice_index"];
}

-(void)play:(NSString *)string

{
    
    if ([_synthesizer isSpeaking]) {
        
        [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        
    }
    
    self.speechStrings = [NSMutableArray arrayWithArray:[string componentsSeparatedByString:@"\n"]];
    [self.speechStrings filterUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
    
    [self startRealPlay];
}

-(void)replay{
    if (!self.speechStrings) {
        return;
    }
    if ([_synthesizer isSpeaking]) {
        [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    }
    if (self.speechStrings.count <= 0) {
        //下一页
        [[NSNotificationCenter defaultCenter] postNotificationName:@"auto_to_next_page" object:nil];
        return;
    }
    [self startRealPlay];
}

-(void)startRealPlay{
    for (NSUInteger i = 0; i < _speechStrings.count; i++) {
        //创建AVSpeechUtterance 对象 用于播放的语音文字
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:_speechStrings[i]];
        //设置使用哪一个国家的语言播放
        utterance.voice = self.voices[_selectVoiceIndex];
        //本段文字播放时的 语速, 应介于AVSpeechUtteranceMinimumSpeechRate 和 AVSpeechUtteranceMaximumSpeechRate 之间
        utterance.rate = self.rate;
        //在播放特定语句时改变声音的声调, 一般取值介于0.5(底音调)~2.0(高音调)之间
        utterance.pitchMultiplier = self.pitchMultiplier;
        //声音大小, 0.0 ~ 1.0 之间
        utterance.volume = self.volume;
        //播放后的延迟, 就是本次文字播放完之后的停顿时间, 默认是0
        utterance.preUtteranceDelay = 0;
        //播放前的延迟, 就是本次文字播放前停顿的时间, 然后播放本段文字, 默认是0
        utterance.postUtteranceDelay = self.postUtteranceDelay;
        [self.synthesizer speakUtterance:utterance];
    }
}

-(void)setRate:(float)rate{
    _rate = rate;
    [self replay];
    [[NSUserDefaults standardUserDefaults] setFloat:rate forKey:@"XSVoiceSynthesizer_voice_rate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setPitchMultiplier:(float)pitchMultiplier{
    _pitchMultiplier = pitchMultiplier;
    [self replay];
    [[NSUserDefaults standardUserDefaults] setFloat:pitchMultiplier forKey:@"XSVoiceSynthesizer_voice_pm"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setSelectVoiceIndex:(NSInteger)selectVoiceIndex{
    _selectVoiceIndex = selectVoiceIndex;
    [self replay];
    [[NSUserDefaults standardUserDefaults] setInteger:selectVoiceIndex forKey:@"XSVoiceSynthesizer_voice_index"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)stop{
    [self.synthesizer stopSpeakingAtBoundary:(AVSpeechBoundaryWord)];
}

-(void)pause{
    [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

-(void)contine{
    [self.synthesizer continueSpeaking];
}

#pragma mark - AVSpeechSynthesizer 代理
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance {
//    NSLog(@"正在朗读:\n%@",utterance.speechString);
    if (self.speechStrings.count > 0) {
        [self.speechStrings removeObjectAtIndex:0];
    }
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance{
//    NSLog(@"朗读wan'c:\n%@",utterance.speechString);
    if (self.speechStrings.count <= 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"auto_to_next_page" object:nil];
    }
}


@end
